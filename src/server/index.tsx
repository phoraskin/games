import * as React from 'react';
import * as express from 'express';
import { renderToString } from 'react-dom/server';
import App from './../shared/App';
import * as serialize from 'serialize-javascript';
import { StaticRouter, matchPath } from 'react-router-dom';
import routes from '../shared/routes';

const server = express();

server.use(express.static('public'));

server.get('*', (req: express.Request, res: express.Response, next: express.NextFunction) => {
  const activeRoute = routes.find(route => !!(matchPath(req.url, route)));

  const promise = activeRoute && activeRoute.fetchInitialData ? activeRoute.fetchInitialData<object>(req.path) : Promise.resolve({});

  promise
    .then((apiResult: object | null) => {
      const context = {};
      const markup = renderToString(
        <StaticRouter location={req.url} context={context}>
          <App />
        </StaticRouter>
      );
    
      res.send(`
        <!DOCTYPE html>
        <html>
          <head>
            <title>The games</title>
          </head>
    
          <body style="background-color: #000">
            <div id="app">${markup}</div>
            <script src="/bundle.js" defer></script>
            <script>window.__INITIAL_DATA__ = ${serialize({apiResult})}</script>
          </body>
        </html>
      `);
    });
});

server.listen(process.env.PORT, () => {
  console.log(`Server is listening on port ${process.env.PORT}`);
});
