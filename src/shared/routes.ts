import Tetris from './components/tetris';
import Dino from './components/dino';
import Mario from './components/mario';

export interface IRoute {
  path: string;
  exact: boolean;
  component: any;
  fetchInitialData?<T>(path:string): Promise<T>;
};

const routes: IRoute[] =  [
  {
    path: '/',
    exact: true,
    component: Tetris,
  },
  {
    path: '/tetris',
    exact: true,
    component: Tetris,
  },
  {
    path: '/dino',
    exact: true,
    component: Dino,
  },
  {
    path: '/mario',
    exact: true,
    component: Mario,
  },
];

export default routes;