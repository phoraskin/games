import * as React from 'react';
import { EGameState } from './../index';

export enum EDinosaurState {STAND, JUMPING, FALLING, DUCK, DIE};
export enum EFootsState {LEFT, RIGHT};

export interface IDinosaurProps {
  offset: number[];
  gameState: EGameState;
  isDevMode: boolean;
};

export interface IDinosaurState {
  dinosaurState: EDinosaurState;
  jumpState: number;
  footsState: EFootsState;
};

class Dinosaur extends React.Component<IDinosaurProps, IDinosaurState> {
  private jumpInterval: number;
  private footsInterval: number;
  private isDownPressing: boolean;

  constructor(props: IDinosaurProps) {
    super(props);

    this.state = {
      dinosaurState: EDinosaurState.STAND,
      jumpState: 0,
      footsState: EFootsState.LEFT,
    };
  }

  componentDidMount() {
    window.addEventListener('keydown', this.handleKeys);
    window.addEventListener('keyup', this.handleKeysUp);
    this.footsInterval = window.setInterval(() => this.moveFoots(), 300);
  }

  componentWillUnmount() {
    window.removeEventListener('keydown', this.handleKeys);
    window.removeEventListener('keyup', this.handleKeysUp);
    window.clearInterval(this.jumpInterval);
    window.clearInterval(this.footsInterval);
  }

  componentDidUpdate(prevProps: IDinosaurProps, prevState: IDinosaurState) {
    if (this.props.gameState === EGameState.OVER && prevProps.gameState !== EGameState.OVER) {
      window.clearInterval(this.jumpInterval);
      window.clearInterval(this.footsInterval);
      this.setState({dinosaurState: EDinosaurState.DIE})
    }
    if (this.props.gameState !== EGameState.OVER && prevProps.gameState === EGameState.OVER) {
      this.footsInterval = window.setInterval(() => this.moveFoots(), 300);
      this.setState({
        dinosaurState: EDinosaurState.STAND,
        jumpState: 0,
      })
    }
  }

  render() {
    const { isDevMode } = this.props;
    const rect = this.getRect();

    return (
      <g>
        <polygon
          points={this.getBorder().map(coords => coords.join(',')).join(' ')}
          fill="#535353"
          stroke="#535353"
        />
        {
          isDevMode && 
            <rect
              x={rect[0][0]}
              y={rect[0][1]}
              width={rect[1][0] - rect[0][0]}
              height={rect[1][1]- rect[0][1]}
              stroke="#ff0000"
              fill="transparent"
            />
        }
      </g>
    )  
  }

  getBorder = (): number[][] => {
    const { jumpState, footsState, dinosaurState } = this.state;
    const { offset } = this.props;
    let border: number[][] = [];
    let additionalOffset = [0, 0];

    const foots = [
      [[24,35],[24,36],[20,36],[20,34],[15,34], [15,36],[13,36],[13,38],[11,38],[11,41],[13,41],[13,42],[10,42],[10,34]],
      [[21,41],[23,41],[23,42],[20,42],[20,36],[18,36],[18,34],[15,34], [11,33],[11,36],[11,38],[15,38],[15,37],[13,37],[13,33],],
    ];

    switch (dinosaurState) {
      case EDinosaurState.STAND:
        border = [
          [8,34],[8,32],[6,32],[6,30],[4,30],[4,28],[2,28],[2,26],[0,26],[0,15],[1,15],[1,19],[3,19],[3,21],[5,21],[5,23],[10,23],[10,21],[12,21],
          [12,19],[15,19],[15,17],[18,17],[18,15],[20,15],[20,2],[22,2],[23,3],[23,5],[26,5],[26,2],[22,2],[22,0],[37,0],[37,2],[39,2],[39,10],
          [29,10],[29,13],[35,13],[35,14],[27,14],[27,19],[31,19],[31,22],[30,22],[30,20],[27,20],[27,27],[25,27],[25,30],[23,30],[23,32],[21,32],
        ];
        break;
      case EDinosaurState.JUMPING:
      case EDinosaurState.FALLING:
        border = [
          [8,34],[8,32],[6,32],[6,30],[4,30],[4,28],[2,28],[2,26],[0,26],[0,15],[1,15],[1,19],[3,19],[3,21],[5,21],[5,23],[10,23],[10,21],[12,21],
          [12,19],[15,19],[15,17],[18,17],[18,15],[20,15],[20,2],[22,2],[23,3],[23,5],[26,5],[26,2],[22,2],[22,0],[37,0],[37,2],[39,2],[39,10],[29,10],
          [29,13],[35,13],[35,14],[27,14],[27,19],[31,19],[31,22],[30,22],[30,20],[27,20],[27,27],[25,27],[25,30],[23,30],[23,32],[21,32],[21,41],
          [23,41],[23,42],[20,42],[20,36],[18,36],[18,34],[15,34],[15,36],[13,36],[13,38],[11,38],[11,41],[13,41],[13,42],[10,42],[10,34],
        ];
        additionalOffset = [0, -jumpState];
        break;
      case EDinosaurState.DUCK:
        border = [
          [11,35],[11,32],[9,32],[9,30],[7,30],[7,28],[5,28],[5,26],[3,26],[3,24],[1,24],[1,22],[-1,22],[-1,17],[0,17],[0,19],[4,19],[4,21],[13,21],[13,19],[29,19],[29,21],
          [34,21],[34,20],[36,20],[36,18],[51,18],[51,20],[37,20],[37,23],[40,23],[40,20],[53,20],[53,28],[43,28],[43,31],[49,31],[49,32],[36,32],[36,30],[31,30],[31,33],[29,33],
          [29,36],[31,36],[31,37],[28,37],[28,33],[22,33],[22,35],
        ];
        break;
      case EDinosaurState.DIE:
        border = [
          [22,0],[37,0],[37,2],[39,2],[39,10],[29,10],[29,13],[35,13],[35,14],[27,14],[27,19],[31,19],[31,22],[30,22],[30,20],[27,20],[27,27],[25,27],
          [25,30],[23,30],[23,32],[21,32],[21,41],[23,41],[23,42],[20,42],[20,36],[18,36],[18,34],[15,34],[15,36],[13,36],[13,38],[11,38],[11,41],[13,41],
          [13,42],[10,42],[10,34],[8,34],[8,32],[6,32],[6,30],[4,30],[4,28],[2,28],[2,26],[0,26],[0,15],[1,15],[1,19],[3,19],[3,21],[5,21],[5,23],
          [10,23],[10,21],[12,21],[12,19],[15,19],[15,17],[18,17],[18,15],[20,15],[20,2],[22,2],[23,3],[23,5],[26,5],[26,4],[25,4],[25,5],[23,5],
          [23,7],[28,7],[28,2],[22,2],
        ];
        additionalOffset = [0, -jumpState];
        break;
    }

    if (dinosaurState === EDinosaurState.STAND || dinosaurState === EDinosaurState.DUCK) {
      border = border.concat(foots[footsState]); 
    }

    border = border.map(coord => {
      coord[0] = coord[0] + offset[0] + additionalOffset[0];
      coord[1] = coord[1] + offset[1] + additionalOffset[1];

      return coord;
    });

    return border;
  }

  getRect = (): number[][] => {
    const { jumpState, dinosaurState } = this.state;
    const { offset } = this.props;
    let polygon: number[][] = [];
    let additionalOffset = [0, 0];

    switch (dinosaurState) {
      case EDinosaurState.STAND:
        polygon = [
          [0,0],[39,42]
        ];
        break;
      case EDinosaurState.JUMPING:
      case EDinosaurState.FALLING:
        polygon = [
          [0,0],[39,42]
        ];
        additionalOffset = [0, -jumpState];
        break;
      case EDinosaurState.DUCK:
        polygon = [
          [-1,17],[53,42]
        ];
        break;
      case EDinosaurState.DIE:
        polygon = [
          [0,0],[39,42],
        ];
        additionalOffset = [0, -jumpState];
        break;
    }

    polygon = polygon.map(coord => {
      coord[0] = coord[0] + offset[0] + additionalOffset[0];
      coord[1] = coord[1] + offset[1] + additionalOffset[1];

      return coord;
    });

    return polygon;
  }

  private handleKeys = (event: KeyboardEvent) => {
    switch (event.key) {
      case 'ArrowUp':
        this.jump();
        break;
      case 'ArrowDown':
        this.handleDownPress();
        break;
    }
  }

  private handleKeysUp = (event: KeyboardEvent) => {
    switch (event.key) {
      case 'ArrowUp':
        // this.jump();
        break;
      case 'ArrowDown':
        this.handleDownUp();
        break;
    }
  }

  private jump = (): void => {
    const { jumpState } = this.state;
    
    if (jumpState > 0 ) return;

    this.setState({dinosaurState: EDinosaurState.JUMPING});

    window.clearInterval(this.jumpInterval);

    this.jumpInterval = window.setInterval(() => {
      this.setState(prevState => {
        const oldJumpState = prevState.jumpState;
        const oldDinosaurState = prevState.dinosaurState;
        let jumpState = oldJumpState;
        let dinosaurState = oldDinosaurState;
        const range = -Math.pow(oldJumpState / 21, 2) + 17.3;

        if (oldDinosaurState === EDinosaurState.FALLING || oldJumpState >= 85) {
          jumpState = oldJumpState - (this.isDownPressing ? 20 : range);
          dinosaurState = EDinosaurState.FALLING;
        } else if (oldDinosaurState === EDinosaurState.JUMPING) {
          jumpState = oldJumpState + range;
        }

        if (jumpState <= 0) {
          window.clearInterval(this.jumpInterval);

          return {
            dinosaurState: this.isDownPressing ? EDinosaurState.DUCK : EDinosaurState.STAND,
            jumpState: 0,
          };
        }

        return {jumpState, dinosaurState};
      });
    }, 25);
  }

  private handleDownPress = (): void => {
    const { dinosaurState } = this.state;
  
    if (this.isDownPressing) return;

    this.isDownPressing = true;

    if (dinosaurState === EDinosaurState.JUMPING) {
      this.setState({dinosaurState: EDinosaurState.FALLING});

      return;
    }

    if (dinosaurState === EDinosaurState.STAND) {
      this.setState({dinosaurState: EDinosaurState.DUCK});

      return;
    }
  }

  private handleDownUp = (): void => {
    const { dinosaurState } = this.state;

    this.isDownPressing = false;

    if (dinosaurState === EDinosaurState.DUCK) {
      this.setState({dinosaurState: EDinosaurState.STAND});
    }
  }

  private moveFoots = (): void => {
    const { gameState } = this.props;
    const { dinosaurState } = this.state;

    if (gameState !== EGameState.ON || dinosaurState !== EDinosaurState.STAND && dinosaurState !== EDinosaurState.DUCK) return;

    this.setState(prevState => ({footsState: prevState.footsState === EFootsState.LEFT ? EFootsState.RIGHT : EFootsState.LEFT}));
  }
}

export default Dinosaur;
