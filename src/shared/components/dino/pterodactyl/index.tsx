import * as React from 'react';
import { EGameState } from './../index';

export enum EPterodactylState {WINGSUP, WINGSDOWN};

export interface IPterodactylProps {
  speed: number;
  id: number;
  gameState: EGameState;
  isDevMode: boolean;
  onMove: (id: number, rect: number[][]) => void;
};

export interface IPterodactylState {
  pterodactylState: EPterodactylState;
  offsetX: number;
  offsetY: number;
};

class Pterodactyl extends React.Component<IPterodactylProps, IPterodactylState> {
  private interval: number;
  private moveInterval: number;

  constructor(props: IPterodactylProps) {
    super(props);

    this.state = {
      pterodactylState: EPterodactylState.WINGSUP,
      offsetX: 650,
      offsetY: 50,
    };
  }

  componentDidMount() {
    this.interval = window.setInterval(() => {
      this.setState(prevState => ({pterodactylState: prevState.pterodactylState === EPterodactylState.WINGSDOWN ? EPterodactylState.WINGSUP: EPterodactylState.WINGSDOWN}));
    }, 300);
    this.moveInterval = window.setInterval(() => this.move(), 25);
  }

  componentWillUnmount() {
    window.clearInterval(this.interval);
    window.clearInterval(this.moveInterval);
  }

  componentDidUpdate(prevProps: IPterodactylProps, prevState: IPterodactylState) {
    if (this.props.gameState === EGameState.OVER && prevProps.gameState !== EGameState.OVER) {
      window.clearInterval(this.interval);
      window.clearInterval(this.moveInterval);
    }
    if (this.props.gameState !== EGameState.OVER && prevProps.gameState === EGameState.OVER) {
      this.interval = window.setInterval(() => {
        this.setState(prevState => ({pterodactylState: prevState.pterodactylState === EPterodactylState.WINGSDOWN ? EPterodactylState.WINGSUP: EPterodactylState.WINGSDOWN}));
      }, 300);
      this.moveInterval = window.setInterval(() => this.move(), 25);
    }
  }

  render() {
    const { isDevMode } = this.props;
    const rect = this.getRect();

    return (
      <g>
        <polygon
          points={this.getBorder().map(coords => coords.join(',')).join(' ')}
          fill="#535353"
          stroke="#535353"
        />
        {
          isDevMode &&
            <rect
              x={rect[0][0]}
              y={rect[0][1]}
              width={rect[1][0] - rect[0][0]}
              height={rect[1][1]- rect[0][1]}
              stroke="#ff0000"
              fill="transparent"
            />
        }
      </g>
    );  
  }

  getBorder = (): number[][] => {
    const { offsetX, offsetY, pterodactylState } = this.state;

    const borders = [
      [
        [14, 0], [15, 0], [15, 2], [17, 2], [17, 4], [19, 4], [19, 6], [21, 6], [21, 8], [23, 8], [23, 10], [25, 10], [25, 12], [27, 12], [27, 16], [29, 16], [29, 18], [41, 18], [41, 19],
        [35, 19], [35, 22], [39, 22], [39, 23], [33, 23], [33, 25], [20, 25], [20, 23], [18, 23], [18, 21], [16, 21], [16, 19], [14, 19], [14, 17], [12, 17], [12, 15], [0, 15], [0, 14],
        [2, 14], [2, 12], [4, 12], [4, 10], [6, 10], [6, 8], [8, 8], [8, 6], [11, 6], [11, 10], [13, 10], [13, 14], [16, 14], [16, 5], [14, 5],
      ],
      [
        [27, 14], [27, 16], [29, 16], [29, 18], [41, 18], [41, 19], [35, 19], [35, 22], [39, 22], [39, 23], [33, 23], [33, 25], [23, 25], [23, 27], [21, 27], [21, 29], [19, 29], [19, 33],
        [17, 33], [17, 35], [16, 35], [16, 19], [14, 19], [14, 17], [12, 17], [12, 15], [0, 15], [0, 14], [2, 14], [2, 12], [4, 12], [4, 10], [6, 10], [6, 8], [8, 8], [8, 6], [11, 6],
        [11, 10], [13, 10], [13, 14],
      ],
    ];

    return borders[pterodactylState].map(coord => {coord[0] = coord[0] + offsetX; coord[1] = coord[1] + offsetY; return coord;});
  }

  getRect = (): number[][] => {
    const { offsetX, offsetY, pterodactylState } = this.state;

    const borders = [
      [[0, 0], [41, 25],],
      [[0, 0], [41, 35],],
    ];

    return borders[pterodactylState].map(coord => {coord[0] = coord[0] + offsetX; coord[1] = coord[1] + offsetY; return coord;});
  }

  private move = () => {
    const rect = this.getRect();
    const speed = this.props.speed;
    const offsetX = -(speed / 100) - 2;

    this.setState(prevState => {
      if (prevState.offsetX < -(rect[1][0] - rect[0][0])) {
        window.clearInterval(this.moveInterval);
        return null;
      }
      return {
        offsetX: prevState.offsetX + offsetX
      };
    }, () => this.props.onMove(this.props.id, this.getRect()));
  }
}

export default Pterodactyl;
