import * as React from 'react';
import { EGameState } from './../index';

export interface IHadoukenProps {
  id: number;
  gameState: EGameState;
  isDevMode: boolean;
  onMove: (id: number, rect: number[][]) => void;
};

export interface IHadoukenState {
  offsetX: number;
  offsetY: number;
};

class Hadouken extends React.Component<IHadoukenProps, IHadoukenState> {
  private moveInterval: number;

  constructor(props: IHadoukenProps) {
    super(props);

    this.state = {
      offsetX: 50,
      offsetY: 25,
    };
  }

  componentDidMount() {
    this.moveInterval = window.setInterval(() => this.move(), 25);
  }

  componentWillUnmount() {
    window.clearInterval(this.moveInterval);
  }

  componentDidUpdate(prevProps: IHadoukenProps, prevState: IHadoukenState) {
    if (this.props.gameState === EGameState.OVER && prevProps.gameState !== EGameState.OVER) {
      window.clearInterval(this.moveInterval);
    }
    if (this.props.gameState !== EGameState.OVER && prevProps.gameState === EGameState.OVER) {
      this.moveInterval = window.setInterval(() => this.move(), 25);
    }
  }

  render() {
    const { isDevMode } = this.props;
    const rect = this.getRect();

    return (
      <g>
        <polygon
          points={this.getBorder(0).map(coords => coords.join(',')).join(' ')}
          fill="#51a4c6"
          stroke="#054f96"
          strokeWidth="6"
        />
        <polygon
          points={this.getBorder(1).map(coords => coords.join(',')).join(' ')}
          fill="#eceaeb"
          stroke="#51a4c6"
          strokeWidth="2"
        />
        {
          isDevMode &&
            <rect
              x={rect[0][0]}
              y={rect[0][1]}
              width={rect[1][0] - rect[0][0]}
              height={rect[1][1]- rect[0][1]}
              stroke="#ff0000"
              fill="transparent"
            />
        }
      </g>
    );  
  }

  getBorder = (ver: number = 0): number[][] => {
    let { offsetX, offsetY } = this.state;

    const borders = [
      [
        [0,49],[0,48],[3,48],[3,44],[5,44],[5,41],[8,41],[8,36],[11,36],[11,32],[9,32],[9,29],[20,29],[20,22],[29,22],[29,20],[30,20],[30,15],[36,15],[36,13],[37,13],[37,12],
        [40,12],[40,11],[42,11],[42,9],[39,9],[39,6],[36,6],[36,2],[41,2],[41,0],[60,0],[60,1],[67,1],[67,5],[73,5],[73,7],[76,7],[76,11],[79,11],[79,13],[85,13],[85,17],[89,17],
        [89,20],[92,20],[92,26],[95,26],[95,35],[99,35],[99,49],
        [99,51],[99,65],[95,65],[95,74],[92,74],[92,80],[89,80],[89,83],[85,83],[85,87],[79,87],[79,89],[76,89],[76,93],[73,93],[73,95],[67,95],[67,99],[60,99],[60,100],[41,100],
        [41,98],[36,98],[36,94],[39,94],[39,91],[42,91],[42,89],[40,89],[40,88],[37,88],[37,87],[36,87],[36,85],[30,85],[30,80],[29,80],[29,78],[20,78],[20,71],[9,71],[9,68],
        [11,68],[11,64],[8,64],[8,59],[5,59],[5,56],[3,56],[3,52],[0,52],[0,51]
      ],
      [
        [30,50],[30,46],[33,46],[33,43],[48,43],[48,41],[42,41],[42,37],[39,37],[39,35],[36,35],[36,30],[48,30],[48,28],[45,25],[45,25],
        [42,25],[42,20],[51,20],[51,17],[64,17],[64,20],[74,20],[74,22],[78,22],[78,26],[80,26],[80,29],[84,29],[84,32],[86,32],[86,38],[90,38],
        [90,62],[86,62],[86,68],[84,68],[84,71],[80,71],[80,74],[78,74],[78,78],[74,78],[74,80],[64,80],[64,83],[51,83],[51,80],[42,80],[42,75],[45,75],[45,75],
        [48,72],[48,70],[36,70],[36,65],[39,65],[39,63],[42,63],[42,59],[48,59],[48,57],[33,57],[33,54],[30,54],[30,50],
      ]
    ];

    // if (ver === 1) {
      offsetX += Math.floor(Math.random() * 5) - 2;
      offsetY += Math.floor(Math.random() * 5) - 2;
    // }

    return borders[ver].map(coord => {coord[0] = coord[0] + offsetX; coord[1] = coord[1] + offsetY; return coord;});
  }

  getRect = (): number[][] => {
    const { offsetX, offsetY } = this.state;

    const border = [[0, 0], [99, 100],];

    return border.map(coord => {coord[0] = coord[0] + offsetX; coord[1] = coord[1] + offsetY; return coord;});
  }

  private move = () => {
    const offsetX = 4;

    this.setState(prevState => {
      if (prevState.offsetX > 600) {
        window.clearInterval(this.moveInterval);
        return null;
      }
      return {
        offsetX: prevState.offsetX + offsetX
      };
    }, () => this.props.onMove(this.props.id, this.getRect()));
  }
}

export default Hadouken;
