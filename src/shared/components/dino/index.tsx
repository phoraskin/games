import * as React from 'react';
import Dinosaur from './dinosaur';
import Pterodactyl from './pterodactyl';
import Cactus, { ECactusType } from './cactus';
import GameOver from './game-over';
import Skyline from './skyline';
import Hadouken from './hadouken';
import { isIntersectionRects, getIntersections } from '../../utils/calc';
import { notEmpty } from '../../utils/array';

import * as styles from './index.css';

export enum EGameState {OFF, ON, OVER};

export interface IDinoProps {
}

export interface IDinoState {
  gameState: EGameState;
  score: number;
  offsetY: number;
  speed: number;
  pterodactyls: IPterodactyl[];
  cactuses: ICactus[];
  isDevMode: boolean;
  isImmortal: boolean;
  hadoukenCount: number;
  hadoukens: IHadouken[]
};

export interface ICactus {
  id: number;
  rect: number[][] | null;
  offsetX: number;
  type?: ECactusType;
};

export interface IPterodactyl {
  id: number;
  rect: number[][] | null;
};

export interface IHadouken {
  id: number;
  rect: number[][] | null;
};

export default class Dino extends React.Component<IDinoProps, IDinoState> {
  private appInterval: number;
  private pterodactylLastId: number;
  private cactusLastId: number;
  private hadoukenLastId: number;
  private dino = React.createRef<Dinosaur>();

  constructor(props: IDinoProps) {
    super(props);

    this.state = {
      gameState: EGameState.OFF,
      score: 0,
      offsetY: 100,
      speed: 600,
      pterodactyls: [],
      cactuses: [],
      hadoukens: [],
      isDevMode: false,
      isImmortal: false,
      hadoukenCount: 3,
    };

    this.pterodactylLastId = 0;
    this.cactusLastId = 0;
    this.hadoukenLastId = 0;
  }

  componentDidMount() {
    window.addEventListener('keydown', this.handleKeys);

    this.appInterval = window.setInterval(this.main, 100);
  }

  componentWillUnmount() {
    window.removeEventListener('keydown', this.handleKeys);
    window.clearInterval(this.appInterval);
  }

  render() {
    const { gameState, offsetY, speed, pterodactyls, cactuses, hadoukens, score, isDevMode } = this.state;

    return (
      <div className={ styles['container'] }>
        <div className={ styles['title'] }>
          <span className={ styles['title__red'] }>T-</span><span className={ styles['title__blue'] }>R</span>ex Runner
        </div>
        <div className={ styles['dino'] }>
          <svg
            className={ styles['dino__svg'] }
            width="600"
            height="150"
            viewBox="0 0 600 150"
          >
            {
              gameState !== EGameState.OFF && (
                <React.Fragment>
                  <Skyline speed={ speed } gameState={ gameState } />
                  {
                    cactuses.map(cactus => (
                      <Cactus
                        key={ cactus.id }
                        id={ cactus.id }
                        offsetY={ offsetY }
                        offsetX={ cactus.offsetX }
                        speed={ speed }
                        onMove={ this.onCactusMove }
                        cactusType={ cactus.type }
                        gameState={ gameState }
                        isDevMode={ isDevMode }
                      />
                    ))
                  }
                  {
                    pterodactyls.map(pterodactyl => (
                      <Pterodactyl
                        key={ pterodactyl.id }
                        id={ pterodactyl.id }
                        speed={ speed }
                        onMove={ this.onPterodactylMove }
                        gameState={ gameState }
                        isDevMode={ isDevMode }
                      />
                    ))
                  }
                </React.Fragment>
              )
            }
            {
              hadoukens.map(hadouken => (
                <Hadouken
                  key={ hadouken.id }
                  id={ hadouken.id }
                  onMove={ this.onHadoukenMove }
                  gameState={ gameState }
                  isDevMode={ isDevMode }
                />
              ))
            }
            <Dinosaur offset={ [30, offsetY] } ref={this.dino} gameState={ gameState } isDevMode={ isDevMode } />
            {
              gameState === EGameState.OVER &&
                <GameOver />
            }
            <text x="530" y="20" className={ styles.score } fill="#535353">{ ('0000' + score).slice(-5) }</text>
          </svg>
        </div>
        <div className={ styles['controls-info'] }>
          <h4 className={ styles['controls-info__title'] }>Управление</h4>
          <div className={ styles['controls-info__record'] }>
            <div>Старт игры/прыжок</div><div>Up</div>
          </div>
          <div className={ styles['controls-info__record'] }>
            <div>Пригнуться</div><div>Down</div>
          </div>
          <div className={ styles['controls-info__record'] }>
            <div>Hadouken(3 заряда)</div><div>Right</div>
          </div>
          <div className={ styles['controls-info__record'] }>
            <div>Режим разработчика(вкл/выкл)</div><div>H</div>
          </div>
        </div>
      </div>
    );
  }

  private handleKeys = (event: KeyboardEvent) => {
    switch (event.key) {
      case 'ArrowUp':
        this.start();
        break;
      case 'ArrowRight':
        this.createHadouken();
        break;
      case 'h':
        this.setState(prevState => ({isDevMode: !prevState.isDevMode}));
        break;
    }
  }

  private start = () => {
    const { gameState, isDevMode } = this.state;

    if (gameState !== EGameState.ON) {
      if (isDevMode) {
        this.setState({
          gameState: EGameState.ON,
          isImmortal: true,
        });
        window.setTimeout(() => {this.setState({isImmortal: false})}, 1000);
      } else {
        this.setState({
          gameState: EGameState.ON,
          cactuses: [],
          pterodactyls: [],
          hadoukens: [],
          speed: 600,
          score: 0,
          hadoukenCount: 3,
        });
      }
    }
  }

  private main = () => {
    const { score, gameState, speed, cactuses, pterodactyls, hadoukens, isImmortal } = this.state;

    const cactusRects = cactuses.map(cactus => cactus.rect).filter(notEmpty);
    const pterodactylRects = pterodactyls.map(pterodactyl => pterodactyl.rect).filter(notEmpty);
    const dinoRect = this.dino.current && this.dino.current.getRect();

    if (gameState !== EGameState.ON || !dinoRect) {
      return;
    }

    this.setState({
      score: score + 1,
      speed: score % 100 === 0 ? speed + 50 : speed,
    });

    if (score % 10 === 0) {
      this.createCactus();
      this.createPterodactyl();
    }

    if (!isImmortal && isIntersectionRects(dinoRect, ...cactusRects, ...pterodactylRects)) {
      this.setState({
        gameState: EGameState.OVER
      });
    }

    if (hadoukens.length) {
      const cactusRects = cactuses.map(cactus => ({id: cactus.id, rect: cactus.rect, type: 'cactus' as 'cactus'}));
      const pterodactylRects = pterodactyls.map(pterodactyl => ({id: pterodactyl.id, rect: pterodactyl.rect, type: 'pterodactyl' as 'pterodactyl'}));
      const rect = hadoukens[0].rect;
      if (notEmpty(rect)) {
        const intersections = getIntersections(rect, ...cactusRects, ...pterodactylRects);
        if (intersections && intersections.length) {
          const pterIds = intersections.filter(el => el.type === 'pterodactyl' && el.rect).map(el => el.id);
          const cactusIds = intersections.filter(el => el.type === 'cactus' && el.rect).map(el => el.id);
          this.setState({
            cactuses: cactuses.filter(el => cactusIds.indexOf(el.id) === -1),
            pterodactyls: pterodactyls.filter(el => pterIds.indexOf(el.id) === -1),
          });
        }
      }
    }
  }

  private createPterodactyl = () => {
    const { pterodactyls, gameState, score } = this.state;

    if (gameState !== EGameState.ON || pterodactyls.length || score < 450) {
      return;
    }

    this.setState({
      pterodactyls: [
        ...pterodactyls,
        {id: ++this.pterodactylLastId, rect: null},
      ]
    });
  }

  private onPterodactylMove = (id: number, rect: number[][]) => {
    const { pterodactyls } = this.state;

    if (rect[1][0] < 0) {
      this.setState({
        pterodactyls: pterodactyls.filter(pterodactyl => pterodactyl.id !== id)
      });
    } else {
      this.setState({
        pterodactyls: pterodactyls.map(pterodactyl => {
          if (pterodactyl.id === id) {
            pterodactyl.rect = rect;
          }

          return pterodactyl;
        })
      });
    }
  }

  private createCactus = () => {
    const { cactuses, gameState, score } = this.state;
    const randomRange = score < 100 ? 5 : (score < 200 ? 8 : 10);
    const random = Math.floor(Math.random() * randomRange);
    let newCactuses: ICactus[] = [];

    if (gameState !== EGameState.ON || cactuses.length > 5 || score < 30) {
      return;
    }

    if ([0, 1, 2, 3, 4, 5].indexOf(random) !== -1) {
      newCactuses = [{id: ++this.cactusLastId, offsetX: 0, rect: null}];
    } else if (random === 6) {
      newCactuses = [
        {id: ++this.cactusLastId, offsetX: 0, type: ECactusType.SECOND, rect: null},
        {id: ++this.cactusLastId, offsetX: 15, type: ECactusType.THIRD, rect: null},
      ];
    } else if ([7, 8].indexOf(random) !== -1) {
      newCactuses = [
        {id: ++this.cactusLastId, offsetX: 0, type: ECactusType.SECOND, rect: null},
        {id: ++this.cactusLastId, offsetX: 15, type: ECactusType.THIRD, rect: null},
        {id: ++this.cactusLastId, offsetX: 30, type: ECactusType.FOURTH, rect: null},
      ];
    } else if (random === 9) {
      newCactuses = [
        {id: ++this.cactusLastId, offsetX: 0, type: ECactusType.SECOND, rect: null},
        {id: ++this.cactusLastId, offsetX: 15, type: ECactusType.THIRD, rect: null},
        {id: ++this.cactusLastId, offsetX: 30, type: ECactusType.FOURTH, rect: null},
        {id: ++this.cactusLastId, offsetX: 45, type: ECactusType.FIRST, rect: null},
      ];
    }

    this.setState({
      cactuses: [
        ...cactuses,
        ...newCactuses
      ]
    });
  }

  private onCactusMove = (id: number, rect: number[][]) => {
    const { cactuses } = this.state;

    if (rect[1][0] < 0) {
      this.setState({
        cactuses: cactuses.filter(cactus => cactus.id !== id)
      });
    } else {
      this.setState({
        cactuses: cactuses.map(cactus => {
          if (cactus.id === id) {
            cactus.rect = rect;
          }

          return cactus;
        })
      });
    }
  }

  private createHadouken = () => {
    const { hadoukens, gameState, hadoukenCount, isDevMode } = this.state;

    if (gameState !== EGameState.ON || hadoukens.length || !isDevMode && hadoukenCount <= 0) {
      return;
    }

    this.setState({
      hadoukenCount: hadoukenCount - 1,
      hadoukens: [
        ...hadoukens,
        {id: ++this.hadoukenLastId, rect: null},
      ]
    });
  }

  private onHadoukenMove = (id: number, rect: number[][]) => {
    const { hadoukens } = this.state;

    if (rect[0][0] > 600) {
      this.setState({
        hadoukens: hadoukens.filter(hadouken => hadouken.id !== id)
      });
    } else {
      this.setState({
        hadoukens: hadoukens.map(hadouken => {
          if (hadouken.id === id) {
            hadouken.rect = rect;
          }

          return hadouken;
        })
      });
    }
  }
}