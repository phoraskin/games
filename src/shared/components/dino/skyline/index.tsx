import * as React from 'react';
import { EGameState } from './../index';
import { getBlockVisibleWidth } from './../../../utils/calc';

export interface ISkylineProps {
  speed: number;
  gameState: EGameState;
};

export interface ISkylineState {
  clouds: number[][][],
  grounds: number[][][],
  dusts: number[][],
};

class Skyline extends React.Component<ISkylineProps, ISkylineState> {
  private moveCloudsInterval: number;
  private createCloudsInterval: number;
  private moveGroundsInterval: number;
  private createGroundsInterval: number;

  constructor(props: ISkylineProps) {
    super(props);

    this.state = {
      clouds: [],
      grounds: [],
      dusts: [],
    };
  }

  componentDidMount() {
    this.generateIntervals();
  }

  componentWillUnmount() {
    window.clearInterval(this.moveCloudsInterval);
    window.clearInterval(this.createCloudsInterval);
    window.clearInterval(this.moveGroundsInterval);
    window.clearInterval(this.createGroundsInterval);
  }

  componentDidUpdate(prevProps: ISkylineProps, prevState: ISkylineState) {
    if (this.props.gameState === EGameState.OVER && prevProps.gameState !== EGameState.OVER) {
      window.clearInterval(this.moveCloudsInterval);
      window.clearInterval(this.createCloudsInterval);
      window.clearInterval(this.moveGroundsInterval);
      window.clearInterval(this.createGroundsInterval);
    }
    if (this.props.gameState !== EGameState.OVER && prevProps.gameState === EGameState.OVER) {
      this.generateIntervals();
    }
  }

  render() {
    const { clouds, grounds, dusts } = this.state;

    return (
      <g>
        {
          clouds.map((cloud, key) => (
            <polygon
              key={ key }
              points={ cloud.map(coords => coords.join(',')).join(' ') }
              fill="transparent"
              stroke="#dddddd"
            />
          ))
        }
        {
          grounds.map((ground, key) => (
            <polyline
              key={ key }
              points={ ground.map(coords => coords.join(',')).join(' ') }
              fill="none"
              stroke="#535353"
              strokeWidth="1"
            />
          ))
        }
        {
          dusts.map((dust, key) => (
            <line
              key={ key }
              x1={ dust[0] }
              y1={ dust[1] }
              x2={ dust[0] + dust[2] }
              y2={ dust[1] }
              fill="none"
              stroke="#535353"
              strokeWidth="1"
            />
          ))
        }
      </g>
    );  
  }

  private generateIntervals = () => {
    this.moveCloudsInterval = window.setInterval(() => this.moveClouds(-2), 30);
    this.moveGroundsInterval = window.setInterval(() => this.moveGrounds(), 25);

    ((timeout?: number) => {
      const test = (timeout = Math.floor(Math.random() * 6000) + 1000) => {
        this.createCloudsInterval = window.setInterval(() => {
          window.clearInterval(this.createCloudsInterval);
          this.setState(prevState => {
            const clouds = prevState.clouds;

            if (clouds.length > 6) return null;

            return {
              clouds: [
                ...clouds,
                this.getCloud(),
              ]
            }
          });
          test();
        }, timeout);
      };
      test(timeout);
    })(1000);

    this.createGroundsInterval = window.setInterval(this.updateGroundsInterval, 500);
  }

  private getCloud = (): number[][] => {
    const cloud = [
      [0, 12], [1, 12], [1, 11], [5, 11], [5, 9], [6, 9], [6, 8], [13, 8], [13, 7], [14, 7], [14, 6], [17, 6], [17, 3], [19, 3], [19, 2], [20, 2], [20, 1], [25, 1], [25, 0], [28, 0],
      [28, 1], [30, 1], [30, 2], [31, 2], [31, 4], [34, 4], [34, 5], [39, 5], [39, 7], [42, 7], [42, 9], [44, 9], [44, 10], [45, 11], [45, 12],
    ];
    const offsetY = Math.floor(Math.random() * 36) + 25;
    const offsetX = 750;

    return cloud.map(coord => {coord[0] = coord[0] + offsetX; coord[1] = coord[1] + offsetY; return coord;});
  }

  private moveClouds = (offsetX: number) => {
    this.setState(prevState => {
      const clouds = prevState.clouds.filter(cloud => cloud.some(coord => coord[0] > 0));
      return {
        clouds: clouds.map(cloud => {
          return cloud.map(coord => {coord[0] = coord[0] + offsetX; return coord;});
        })
      };
    });
  }

  private getGround = (offsetX: number): number[][] => {
    let ground;
    const randomVariant = Math.floor(Math.random() * 5);
    const randomLength = Math.floor(Math.random() * 225) + 4;
    const grounds = [
      [[0,0],[0,-1],[1,-1],[1,-2],[3,-2],[3,-3],[4,-3],[4,-4],[9,-4],[9,-3],[10,-3],[10,-2],[11,-2],[12,-1],[13,-1],[13,0],[20,0],],
      [[0,0],[0,1],[1,1],[1,2],[9,2],[9,1],[10,1],[10,0],[17,0]],
    ];
    const offsetY = 130;

    if (randomVariant < grounds.length) {
      ground = grounds[randomVariant];
    } else {
      ground = [[0,0],[randomLength,0]];
    }

    return ground.map(coord => {coord[0] = coord[0] + offsetX; coord[1] = coord[1] + offsetY; return coord;});
  }

  private getDust = (): number[] => {
    const randomLength = Math.floor(Math.random() * 8) + 1;
    const offsetX = 650;
    const offsetY = Math.floor(Math.random() * 5) + 135;

    return [offsetX, offsetY, randomLength];
  }

  private updateGroundsInterval = () => {
    this.setState(prevState => {
      let grounds = prevState.grounds;
      const dusts = prevState.dusts;
      let width = grounds.reduce<number>((prev, curr) => prev + getBlockVisibleWidth(curr), 0);

      while (width < 820) {
        const newGround = this.getGround(width);
        width += getBlockVisibleWidth(newGround);
        grounds = [
          ...grounds,
          newGround,
        ]
      }

      if (width > 820) {
        return {
          grounds,
          dusts: dusts.length < 20 ? [...dusts, this.getDust()] : dusts,
        };
      }

      return {
        grounds,
        dusts: dusts.length < 20 ? [...dusts, this.getDust()] : dusts,
      };
    });
  }

  private moveGrounds = () => {
    const speed = this.props.speed;
    const offsetX = -(speed / 100);
    this.setState(prevState => {
      const grounds = prevState.grounds.filter(ground => ground.some(coord => coord[0] > 0));
      const dusts = prevState.dusts.filter(dust => dust[0] + dust[2] > 0);
      return {
        grounds: grounds.map(ground => {
          return ground.map(coord => {coord[0] = coord[0] + offsetX; return coord;});
        }),
        dusts: dusts.map(dust => {dust[0] = dust[0] + offsetX; return dust;}),
      };
    });
  }
}

export default Skyline;
