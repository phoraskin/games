import * as React from 'react';
import { EGameState } from './../index';

export enum ECactusType {FIRST, SECOND, THIRD, FOURTH};

export interface ICactusProps {
  cactusType?: ECactusType;
  offsetY: number;
  offsetX: number;
  speed: number;
  id: number;
  gameState: EGameState;
  isDevMode: boolean;
  onMove: (id: number, rect: number[][]) => void;
};

export interface ICactusState {
  cactusType: ECactusType;
  offsetX: number;
};

class Cactus extends React.Component<ICactusProps, ICactusState> {
  private moveInterval: number;

  constructor(props: ICactusProps) {
    super(props);

    this.state = {
      cactusType: props.cactusType ||  Math.floor(Math.random() * (Object.keys(ECactusType).length / 2)),
      offsetX: 650 + props.offsetX,
    };
  }

  componentDidMount() {
    this.moveInterval = window.setInterval(() => this.move(), 25);
  }

  componentWillUnmount() {
    window.clearInterval(this.moveInterval);
  }

  componentDidUpdate(prevProps: ICactusProps, prevState: ICactusState) {
    if (this.props.gameState === EGameState.OVER && prevProps.gameState !== EGameState.OVER) {
      window.clearInterval(this.moveInterval);
    }
    if (this.props.gameState !== EGameState.OVER && prevProps.gameState === EGameState.OVER) {
      this.moveInterval = window.setInterval(() => this.move(), 25);
    }
  }

  render() {
    const { isDevMode } = this.props;
    const rect = this.getRect();

    return (
      <g>
        <polygon
          points={this.getBorder().map(coords => coords.join(',')).join(' ')}
          fill="#535353"
          stroke="#535353"
        />
        {
          isDevMode &&
            <rect
              x={rect[0][0]}
              y={rect[0][1]}
              width={rect[1][0] - rect[0][0]}
              height={rect[1][1]- rect[0][1]}
              stroke="#ff0000"
              fill="transparent"
            />
        }
      </g>
    );  
  }

  getBorder = (): number[][] => {
    const { cactusType } = this.state;
    const { offsetY } = this.props;
    const { offsetX } = this.state;

    const cactuses = [
      [
        [9, 0], [12, 0], [12, 1], [13, 1], [13, 26], [18, 26], [18, 11], [19, 11], [19, 10], [20, 10], [20, 11], [21, 11], [21, 25], [20, 25], [20, 26], [19, 26], [19, 27],
        [18, 27], [18, 28], [17, 28], [17, 29], [13, 29], [13, 45], [6, 45], [8, 44], [8, 30], [3, 30], [3, 29], [2, 29], [2, 28], [1, 28], [1, 27], [0, 27], [0, 13], [1, 13], [1, 12],
        [2, 12], [2, 13], [3, 13], [3, 26], [8, 26], [8, 1], [9, 1],
      ],
      [
        [6, 0], [8, 0], [8, 1], [9, 1], [9, 15], [12, 15], [12, 5], [13, 5], [13, 4], [13, 5], [14, 5], [14, 15], [13, 15], [13, 16], [12, 16], [12, 17], [9, 17], [9, 32], [5, 32],
        [5, 21], [2, 21], [2, 20], [1, 20], [1, 19], [0, 19], [0, 9], [1, 9], [1, 8], [1, 9],  [2, 9], [2, 19], [5, 19], [5, 1], [6, 1],
      ],
      [
        [6, 0], [8, 0], [8, 1], [9, 1], [9, 22], [12, 22], [12, 8], [13, 8], [13, 7], [13, 8], [14, 8], [14, 22], [13, 22], [13, 23], [12, 23], [12, 24], [9, 24], [9, 32], [5, 32],
        [5, 22], [3, 22], [3, 21], [2, 21], [2, 20], [1, 20], [1, 19], [0, 19], [0, 4], [1, 4], [1, 3], [1, 4], [2, 4], [2, 20], [5, 20], [5, 1], [6, 1],
      ],
      [
        [6, 0], [8, 0], [8, 1], [9, 1], [9, 17], [12, 17], [12, 5], [13, 5], [13, 4], [13, 5], [14, 5], [14, 17], [13, 17], [13, 18], [12, 18], [12, 19], [9, 19], [9, 32], [5, 32],
        [5, 17], [2, 17], [2, 16], [1, 16], [1, 15], [0, 15], [0, 4], [1, 4], [1, 3], [1, 4], [2, 4], [2, 15], [5, 15], [5, 1], [6, 1],
      ],
    ];

    return cactuses[cactusType].map(coord => {coord[0] = coord[0] + offsetX; coord[1] = coord[1] + offsetY; return coord;});
  }

  getRect = (): number[][] => {
    const { cactusType } = this.state;
    const { offsetY } = this.props;
    const { offsetX } = this.state;

    const cactuses = [
      [[0, 0], [21, 45],],
      [[0, 0], [14, 32],],
      [[0, 0], [14, 32],],
      [[0, 0], [14, 32],],
    ];

    return cactuses[cactusType].map(coord => {coord[0] = coord[0] + offsetX; coord[1] = coord[1] + offsetY; return coord;});
  }

  private move = () => {
    const rect = this.getRect();
    const speed = this.props.speed;
    const offsetX = -(speed / 100);

    this.setState(prevState => {
      if (prevState.offsetX < -(rect[1][0] - rect[0][0])) {
        window.clearInterval(this.moveInterval);
        return null;
      }
      return {
        offsetX: prevState.offsetX + offsetX
      };
    }, () => this.props.onMove(this.props.id, this.getRect()));
  }
}

export default Cactus;
