declare const styles: {
  'container': string;
  'title': string;
  'title__red': string;
  'title__blue': string;
  'dino': string;
  'dino__svg': string;
  'score': string;
  'controls-info': string;
  'controls-info__title': string;
  'controls-info__record': string;
};

export = styles;
