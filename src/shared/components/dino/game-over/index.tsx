import * as React from 'react';

const GameOver = () => {
  const arrow = [[14,5], [18,9], [14,13], [14,10], [9,10], [9,19], [24,19], [24,10], [22,10], [22,8], [24,8], [26,10], [26,19], [24,21], [9,21], [7,19], [7,10], [9,8], [14,8],];
  const center = [300, 75];
  const rectWidth = 34;
  const rectHeight = 30;
  const rectRadius = 3;
  const arrowPoints = arrow.map(coord => `${coord[0] + center[0] - rectWidth / 2},${coord[1] + center[1] - rectHeight / 2}`).join(' ');
  const letterSpace = 12;
  const letterWidth = 11;
  const gameOver = [
    [[3,0], [10,0], [10,1], [4,1], [4,2], [2,2], [2,8], [4,8], [4,9], [8,9], [8,5], [6,5], [10,5], [10,10], [3,10], [3,8], [2,8], [2,7], [0,7], [0,3], [2,3], [2,2], [3,2],], // g
    [[3,0], [7,0], [7,2], [8,2], [8,3], [10,3], [10,10], [8,10], [8,2], [6,2], [6,1], [4,1], [4,2], [2,2], [2,6], [7,6], [7,7], [2,7], [2,10], [0,10], [0,3], [2,3], [2,2], [3,2],], // a
    [[0,0], [2,0], [2,2], [4,2], [4,3], [6,3], [6,2], [8,2], [8,0], [10,0], [10,10], [8,10], [8,5], [5,5], [5,7], [5,5], [2,5], [2,10], [0,10],], // m
    [[0,0], [10,0], [10,1], [2,1], [2,5], [8,5], [2,5], [2,9], [10,9], [10,10], [0,10],], // e
    [],
    [[2,0], [8,0], [8,2], [10,2], [10,8], [8,8], [8,10], [2,10], [2,8], [0,8], [0,2], [2,2], [2,9], [8,9], [8,1], [2,1],], // o
    [[0,0], [2,0], [2,5], [4,5], [4,6], [6,6], [6,5], [8,5], [8,0], [10,0], [10,5], [8,5], [8,7], [7,7], [7,8], [5,8], [5,10], [5,8], [3,8], [3,7], [2,7], [2,5], [0,5],], // v
    [[0,0], [10,0], [10,1], [2,1], [2,5], [8,5], [2,5], [2,9], [10,9], [10,10], [0,10],], // e
    [[0,0], [8,0], [8,2], [10,2], [10,5], [7,5], [7,8], [8,8], [8,9], [9,9], [10,9], [10,10], [6,10], [6,8], [5,8], [5,7], [3,7], [3,6], [6,6], [6,5], [8,5], [8,1], [2,1], [2,10], [0,10],], // r
  ];
  const getTextWidth = () => gameOver.reduce<number>((prev, curr, index) => prev + letterWidth + (index !== gameOver.length - 1 ? letterSpace : 0), 0);

  return (
    <g>
      <g fill="#535353" stroke="#535353">
        {
          gameOver.map((letter, key) => (
            <polygon
              key={ key }
              points={ letter.map(coord => `${coord[0] + center[0] - getTextWidth() / 2 + (letterSpace + letterWidth) * key}, ${coord[1] + center[1] - rectHeight - 20}`).join(' ') }
            />
          ))
        }
      </g>
      <rect
        x={ center[0] - rectWidth / 2 }
        y={ center[1] - rectHeight / 2 }
        rx={ rectRadius }
        width={ rectWidth }
        height={ rectHeight }
        fill="#535353"
      />
      <polygon
        points={ arrowPoints }
        fill="#ffffff"
        stroke="#ffffff"
      />
    </g>
  );
};

export default GameOver;
