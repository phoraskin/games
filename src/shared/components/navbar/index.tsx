import * as React from 'react';
import { NavLink } from 'react-router-dom';
import * as styles from './index.css';

export default function Navbar () {
  return (
    <ul className={styles['navbar']}>
      <li>
        <NavLink activeStyle={{fontWeight: 'bold'}} to={`/tetris`}>
          Tetris
        </NavLink>
      </li>
      <li>
        <NavLink activeStyle={{fontWeight: 'bold'}} to={`/dino`}>
          T-Rex Runner
        </NavLink>
      </li>
      <li>
        <NavLink activeStyle={{fontWeight: 'bold'}} to={`/mario`}>
          Mario
        </NavLink>
      </li>
    </ul>
  )
}