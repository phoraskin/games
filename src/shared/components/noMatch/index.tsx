import * as React from 'react';
import * as styles from './index.css';

export default class NoMatch extends React.Component<{}, {}> {
  render() {
    return (
      <div className={styles['no-match']}>
        Page not exist
      </div>
    )
  }
};