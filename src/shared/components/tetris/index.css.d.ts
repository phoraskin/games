declare const styles: {
  'title': string;
  'tetris': string;
  'screen': string;
  'indicator': string;
  'indicator__label': string;
  'indicator__data': string;
};

export = styles;
