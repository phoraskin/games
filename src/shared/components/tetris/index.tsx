import * as React from 'react';
import Piece, { EPieceType, EDirection } from './piece';
import Block, { width as blockWidth, height as blockHeight } from './block';
import * as styles from './index.css';

export interface ITetrisProps {
  languages?: string[];
}

export interface ITetrisState {
  pieces: Piece[];
  isGameOver: boolean;
  score: number;
};

export default class Tetris extends React.Component<ITetrisProps, ITetrisState> {
  private fallingInterval: number;
  private columns: number = 10;
  private rows: number = 20;

  constructor(props: ITetrisProps) {
    super(props);

    this.state = {
      pieces: [],
      isGameOver: false,
      score: 0,
    };
  }

  componentDidMount() {
    this.startGame();
  }

  componentWillUnmount() {
    window.clearInterval(this.fallingInterval);
    window.removeEventListener('keydown', this.handleKeys);
  }

  render() {
    const { pieces, isGameOver, score } = this.state;
    const { columns, rows } = this;

    return (
      <div>
        <div className={ styles['title'] }>
          Tetris game
        </div>
        <div className={ styles['tetris'] }>
          <div className="sidebar">
            <div className={ styles['indicator'] }>
              <div className={ styles['indicator__label'] }>Game is</div>
              <div className={ styles['indicator__data'] }>
                {
                  isGameOver ? (
                    <div>
                      <div>Over</div>
                      <button onClick={ this.startGame }>Start new</button>
                    </div>
                  ) : 'Running'
                }
              </div>
            </div>
          </div>
          <svg
            className={ styles['screen'] }
            style={ {width: `${blockWidth * columns}px`, height: `${blockHeight * rows}px`} }
          >
            {
              pieces.map(piece => piece.render())
            }
          </svg>
          <div className="sidebar">
            <div className={ styles['indicator'] }>
              <div className={ styles['indicator__label'] }>Score</div>
              <div className={ styles['indicator__data'] }>{ score }</div>
            </div>
          </div>
        </div>
      </div>
    );
  }

  private setPiece = (id: string, piece: Piece) => {
    this.setState({
      pieces: this.state.pieces.map(el => {
        if (el.getId() === id) {
          el.setBlocks(piece.getBlocks());
          el.setIsActive(piece.getIsActive())
        };

        return el;
      }),
    });
  }

  private onFalling = () => {
    const { pieces } = this.state;
    const activePiece = pieces.find(piece => piece.getIsActive());

    if (!activePiece) return;

    this.clearLines();
    this.moveDown();
  }

  private handleKeys = (event: KeyboardEvent) => {
    const { pieces } = this.state;
    const activePiece = pieces.find(piece => piece.getIsActive());

    if (!activePiece) return;

    switch (event.key) {
      case 'ArrowLeft':
        this.moveLeftRight(-1);
        break;
      case 'ArrowRight':
        this.moveLeftRight(1);
        break;
      case 'ArrowUp':
        this.rotate();
        break;
      case 'ArrowDown':
        this.moveDown();
        break;
    }
  }

  private isTouching = (piece: Piece): boolean => {
    const allBlocks = this.getAllBlocks([piece.getId()]);

    return piece.getBlocks().some(block => {
      // Касается другого блока
      if (allBlocks.some(el => (block.getY() >= el.getY() - 1) && (block.getX() === el.getX()))) {
        return true;
      }

      // Касается дна
      return block.getY() >= this.rows - 1;
    });
  }

  // Касается ли вершины
  private isOver = (): boolean => {
    const { pieces } = this.state;
    const activePiece = pieces.find(piece => piece.getIsActive());

    if (!activePiece) return false;

    return activePiece.getBlocks().some(block => {
      return block.getY() <= 0;
    });
  }

  private generatePiece = () => {
    const max = Object.keys(EPieceType).length / 2;
    const pieceTypeIndex = Math.floor(Math.random() * max);
    const piece = new Piece(pieceTypeIndex);

    // Сместим в серидину и вверх
    piece.setBlocks(piece.getBlocks().map(block => {
      block.setX(block.getX() + (this.columns / 2) - 1);
      block.setY(block.getY() - 2);

      return block;
    }))

    return piece;
  }

  private getAllBlocks = (exceptPieceIds: string[] = []): Block[] => {
    let { pieces } = this.state;

    if (exceptPieceIds.length) {
      pieces = pieces.filter(piece => exceptPieceIds.indexOf(piece.getId()) === -1);
    }

    return pieces.reduce<Block[]>((prev, curr) => prev.concat(curr.getBlocks()), []);
  }

  private clearLines = () => {
    const { pieces } = this.state;
    const activePiece = pieces.find(piece => piece.getIsActive());
    const deletedLines: number[] = [];
    const lines: {blockId: string; piece: Piece;}[][] = [];

    pieces.forEach(piece => {
      const pieceId = piece.getId();

      if (activePiece && (pieceId === activePiece.getId())) return;

      piece.getBlocks().forEach(block => {
        const y = block.getY();
        const blockId = block.getId();
        
        if (lines[y]) 
          lines[y].push({blockId, piece})
        else
          lines[y] = [{blockId, piece}];
      });
    });

    lines.forEach((line, key) => {
      if (line.length >= this.columns) {
        line.forEach(({blockId, piece}) => {
          piece.deleteBlock(blockId);
        });
        deletedLines.push(key);
      }
    });

    if (!deletedLines.length) return;

    // Добавим очки
    this.setState(prevState => ({score: prevState.score + deletedLines.length * 100}));

    // Опустим блоки вниз
    deletedLines.forEach(index => {
      lines.forEach((line, lineIndex) => {
        if (lineIndex < index) {
          line.forEach(({blockId, piece}) => {
            piece.downBlock(blockId);
          })
        }
      });
    });
  }

  private moveLeftRight = (offset: number) => {
    const { pieces } = this.state;
    const activePiece = pieces.find(piece => piece.getIsActive());

    if (!activePiece) return;

    const activeBlocks = activePiece.getBlocks();
    const allBlocks = this.getAllBlocks([activePiece.getId()]);

    const isTouching = activeBlocks.some(block => {
      if (offset > 0) {
        if ((block.getX() + offset) >= this.columns) return true;
        
        // Касается другого блока
        if (allBlocks.some(el => (block.getY() === el.getY()) && (el.getX() - block.getX() === 1))) {
          return true;
        }
      } else {
        if ((block.getX() + offset) < 0) return true;
        
        // Касается другого блока
        if (allBlocks.some(el => (block.getY() === el.getY()) && (el.getX() - block.getX() === -1))) {
          return true;
        }
      }

      return false;
    });
    
    if (isTouching) return;

    activePiece.move(EDirection.RIGHT, offset);

    this.setPiece(activePiece.getId(), activePiece);
  }

  private moveDown = () => {
    const { pieces } = this.state;
    const activePiece = pieces.find(piece => piece.getIsActive());

    if (!activePiece) return;

    if (this.isTouching(activePiece)) {
      if (this.isOver()) {
        this.finishGame();

        return;
      }

      activePiece.setIsActive(false);
      this.setState(prevState => {
        return {pieces: [
          ...prevState.pieces.map(el => {
            if (el.getId() === activePiece.getId()) return activePiece;
    
            return el;
          }),
          this.generatePiece()
        ]};
      });

      return;
    } 

    activePiece.move(EDirection.DOWN);

    this.setPiece(activePiece.getId(), activePiece);
  }

  private rotate = () => {
    const { pieces } = this.state;
    const activePiece = pieces.find(piece => piece.getIsActive());

    if (!activePiece) return;

    const resultBlocks: Block[] = [];
    const blocks = activePiece.getBlocks();
    let diffX: number, diffY: number;

    blocks.forEach((block, key) => {
      diffX = diffX || block.getX();
      diffY = diffY || block.getY();
      const newX: number = (block.getY() - diffY) * -1 + diffX;
      const newY: number = (block.getX() - diffX) + diffY;
      resultBlocks.push(new Block({x: newX, y: newY, color: block.getColor()}));
    });

    activePiece.setBlocks(resultBlocks);
    this.setPiece(activePiece.getId(), activePiece);
  }

  private startGame = () => {
    this.setState({
      pieces: [this.generatePiece()],
      isGameOver: false,
      score: 0,
    });
    this.fallingInterval = window.setInterval(this.onFalling, 1000);
    window.addEventListener('keydown', this.handleKeys);
  }

  private finishGame = () => {
    window.clearInterval(this.fallingInterval);
    window.removeEventListener('keydown', this.handleKeys);
    this.setState({isGameOver: true});
  }
}