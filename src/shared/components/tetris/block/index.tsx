import * as React from 'react';
import { v1 } from 'uuid';

export interface IOptions {
  x: number,
  y: number,
  color: string;
};

export const width = 24;
export const height = 24;

export default class Block {
  private id: string;
  private x: number;
  private y: number;
  private color: string;

  constructor(options: IOptions) {
    const { x, y, color } = options;

    if (typeof x !== 'number') throw new Error('x is not number');

    if (typeof y !== 'number') throw new Error('y is not number');

    if (!color) throw new Error('color is not passed');

    this.id = v1();
    this.x = x;
    this.y = y;
    this.color = color;
  }

  render = (): JSX.Element => {
    const { id } = this;
    return (
      <rect
        id={ id }
        x={ `${this.getX() * width}px` }
        y={ `${this.getY() * height}px` }
        width={ `${width}px` }
        height={ `${height}px` }
        style={ {fill: this.getColor(), stroke: '#000', strokeWidth: '0.5'} }
        key={ this.id }
      >
        1
      </rect>
    );
  }

  getId = (): string => {
    return this.id;
  }

  getX = (): number => {
    return this.x;
  }

  setX = (x: number) => {
    this.x = x;
  }

  getY = (): number => {
    return this.y;
  }

  setY = (y: number) => {
    this.y = y;
  }

  getColor = (): string => {
    return this.color;
  }
}