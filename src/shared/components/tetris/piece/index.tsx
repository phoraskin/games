import * as React from 'react';
import Block from './../block';
import { v1 } from 'uuid';

export enum EPieceType {TRIANLE, RECTANGLE, LINE, S, Z, L, L_FLIPPED};

export enum EDirection {DOWN, LEFT, RIGHT, TOP};

export default class Piece {
  private id: string;
  private type: EPieceType;
  private blocks: Block[];
  private isActive: boolean;

  constructor(type: EPieceType) {
    this.type = type;
    this.id = v1();

    switch (type) {
      case EPieceType.TRIANLE:
        this.blocks = [
          new Block({x: 1, y: 0, color: 'orange'}), new Block({x: 0, y: 1, color: 'orange'}), new Block({x: 1, y: 1, color: 'orange'}), new Block({x: 2, y: 1, color: 'orange'}),
        ];
        break;
      case EPieceType.RECTANGLE:
        this.blocks = [
          new Block({x: 0, y: 0, color: 'skyblue'}), new Block({x: 1, y: 0, color: 'skyblue'}), new Block({x: 0, y: 1, color: 'skyblue'}), new Block({x: 1, y: 1, color: 'skyblue'}),
        ];
        break;
      case EPieceType.LINE:
        this.blocks = [
          new Block({x: 0, y: 0, color: 'red'}), new Block({x: 1, y: 0, color: 'red'}), new Block({x: 2, y: 0, color: 'red'}), new Block({x: 3, y: 0, color: 'red'}),
        ];
        break;
      case EPieceType.S:
        this.blocks = [
          new Block({x: 0, y: 1, color: 'hotpink'}), new Block({x: 1, y: 1, color: 'hotpink'}), new Block({x: 1, y: 0, color: 'hotpink'}), new Block({x: 2, y: 0, color: 'hotpink'}),
        ];
        break;
      case EPieceType.Z:
        this.blocks = [
          new Block({x: 0, y: 0, color: 'rebeccapurple'}), new Block({x: 1, y: 0, color: 'rebeccapurple'}), new Block({x: 1, y: 1, color: 'rebeccapurple'}), new Block({x: 2, y: 1, color: 'rebeccapurple'}),
        ];
        break;
      case EPieceType.L:
        this.blocks = [
          new Block({x: 0, y: 1, color: 'salmon'}), new Block({x: 1, y: 1, color: 'salmon'}), new Block({x: 2, y: 0, color: 'salmon'}), new Block({x: 2, y: 1, color: 'salmon'}),
        ];
        break;
      case EPieceType.L_FLIPPED:
        this.blocks = [
          new Block({x: 0, y: 0, color: 'tomato'}), new Block({x: 0, y: 1, color: 'tomato'}), new Block({x: 1, y: 1, color: 'tomato'}), new Block({x: 2, y: 1, color: 'tomato'}),
        ];
        break;
    }

    this.isActive = true;

    console.log(EPieceType[type], this.blocks.map(block => (`${block.getX()} ${block.getY()}`)).join('; '));
  }

  render = (): JSX.Element => {
    const { blocks } = this;

    return (
      <React.Fragment>
        {
          blocks.map(block => block.render())
        }
      </React.Fragment>
    );
  }

  getId = (): string => {
    return this.id;
  }

  getType = (): EPieceType => {
    return this.type;
  }

  getIsActive = (): boolean => {
    return this.isActive;
  }

  setIsActive = (isActive: boolean) => {
    this.isActive = isActive;
  }

  getBlocks = (): Block[] => {
    return this.blocks;
  }

  setBlocks = (blocks: Block[]) => {
    this.blocks = blocks;
  }

  move = (direction: EDirection, offset: number = 1) => {
    switch (direction) {
      case EDirection.DOWN:
        this.blocks = this.blocks.map(block => {
          block.setY(block.getY() + offset);
          return block;
        });
        break;
      case EDirection.LEFT:
        this.blocks = this.blocks.map(block => {
          block.setX(block.getX() - offset);
          return block;
        });
        break;
      case EDirection.RIGHT:
        this.blocks = this.blocks.map(block => {
          block.setX(block.getX() + offset);
          return block;
        });
        break;
    }
  }

  deleteBlock = (blockId: string) => {
    this.blocks = this.blocks.filter(block => block.getId() !== blockId);
  }

  downBlock = (blockId: string) => {
    this.blocks = this.blocks.map(block => {
      if (block.getId() === blockId) {
        block.setY(block.getY() + 1);
      }

      return block;
    });
  }
}