import * as React from 'react';
import GameLoop from './components/GameLoop';
import Controls from './components/Controls';
import Lib from './components/scenes/Lib';
import Menu from './components/scenes/Menu';
import Game from './components/scenes/Game';
import GameOver from './components/scenes/GameOver';
import GameWin from './components/scenes/GameWin';
import { IScene } from './components/scenes/Scene';

import * as styles from './index.css';

export interface IImgs {[name: string]: HTMLImageElement};

export type TSceneName = 'lib' | 'menu' | 'game' | 'gameOver' | 'gameWin';

export interface IScreen {
  imgs: IImgs;
  canvas: HTMLCanvasElement;
};

export interface IMarioProps {
}

export interface IMarioState {
};

export default class Mario extends React.Component<IMarioProps, IMarioState> {
  private canvas = React.createRef<HTMLCanvasElement>();
  private controls: Controls;

  constructor(props: IMarioProps) {
    super(props);

    this.state = {
    };
  }

  componentDidMount() {
    if (!this.canvas.current) {
      return;
    }

    const ctx = this.canvas.current.getContext('2d');
    ctx && ctx.scale(1.5, 1.5);

    const loop = new GameLoop();
    const screen: IScreen = {
      imgs: {},
      canvas: this.canvas.current,
    };
    this.controls = new Controls();

    const scenes: {[key in TSceneName]?: IScene} = {
      lib: new Lib(screen, this.controls),
      menu: new Menu(screen, this.controls),
      game: new Game(screen, this.controls),
      gameOver: new GameOver(screen, this.controls),
      gameWin: new GameWin(screen, this.controls),
    };

    let currentScene: TSceneName = 'lib';
    loop.start(time => {
      const scene = scenes[currentScene];
      if (scene) {
        const sceneName = scene.render(time);
        if (sceneName) {
          currentScene = sceneName;
        }
      }
    });
  }

  componentWillUnmount() {
    this.controls.desubscribe();
  }

  render() {
    const {  } = this.state;

    return (
      <div className={ styles['container'] }>
        <div className={ styles['title'] }>В разработке</div>
        <div className={ styles['canvas'] }>
          <canvas id="screen" width="800" height="350" style={{width: '800px', height: '350px'}} ref={this.canvas} />
        </div>
      </div>
    );
  }
}