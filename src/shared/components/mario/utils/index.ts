export interface IBlock {
  x: number;
  y: number;
  width: number;
  height: number;
};

export const debounce = (func: Function, timeout: number) => {
  let isRunning: boolean = false;

  return (...args: any[]) => {
    if (isRunning) {
      return;
    }

    isRunning = true;
    window.setTimeout(() => {
      isRunning = false;
    }, timeout);

    return func(...args);
  };
};

export const throttle = (func: Function, timeout: number) => {
  let isRunning: boolean = false;
  let lastArgs: any[] | null = null;

  return function (...args: any[]) {
    lastArgs = args;

    if (isRunning) {
      return;
    }

    isRunning = true;
    window.setTimeout(() => {
      isRunning = false;

      if (lastArgs) {
        func(lastArgs);
        lastArgs = null;
      }
    }, timeout);

    func(lastArgs);
    lastArgs = null;
  };
};

/**
 * Пересекаются ли блоки
 * @param block1 блок1
 * @param block2 блок 2
 */
export const isIntersect = (block1: IBlock, block2: IBlock): boolean => {
  const isHCross = block2.x < (block1.x + block1.width) && block2.x >= block1.x || ((block2.x + block2.width) > block1.x && block2.x <= block1.x) || block2.x === block1.x && (block2.x + block2.width) === (block1.x + block1.width);
  const isVCross = block2.y < (block1.y + block1.height) && block2.y >= block1.y || ((block2.y + block2.height) > block1.y && block2.y <= block1.y) || block2.y === block1.y && (block2.y + block2.height) === (block1.y + block1.height);

  return isHCross && isVCross;
}