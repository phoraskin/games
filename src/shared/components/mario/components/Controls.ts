export type TStateName = 'left' | 'right' | 'up' | 'down' | 'fire';

export interface IControls {
  states: {[key in TStateName]: boolean};
  codes: {[key: number]: TStateName};
};

export default class Controls implements IControls {
  public states: {[key in TStateName]: boolean};
  codes: {[key: number]: TStateName}

  constructor() {
    this.states = {left: false, right: false, up: false, down: false, fire: false};
    this.codes = {37: 'left', 39: 'right', 38: 'up', 40: 'down', 32: 'fire'};

    document.addEventListener('keydown', this.onKeyDown);
    document.addEventListener('keyup', this.onKeyUp);
  }

  private onKeyDown = (e: KeyboardEvent) => {
    this.onKey(true, e);
  }

  private onKeyUp = (e: KeyboardEvent) => {
    this.onKey(false, e);
  }

  private onKey = (val: boolean, e: KeyboardEvent) => {
    const stateName = this.codes[e.keyCode];

    if (stateName) {
      e.preventDefault && e.preventDefault();
      e.stopPropagation && e.stopPropagation();

      this.states[stateName] = val;
    }
  }

  public desubscribe = () => {
    document.removeEventListener('keydown', this.onKeyDown);
    document.removeEventListener('keyup', this.onKeyUp);
  }
}