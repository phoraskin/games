import Game from "./scenes/Game";

export default class Camera {
  public x: number;
  public y: number;
  private scene: Game;

  constructor(x: number, y: number, scene: Game) {
    this.x = x;
    this.y = y;
    this.scene = scene;
  }

  public update = (time: number) => {
    if ((this.scene.player.x - this.x) > 200) {
      this.x = this.scene.player.x - 200;
    }
    if (this.x > 2860) {
      this.x = 2860;
    }
  }
}