import { IScreen, IImgs } from '..';
import { IControls } from './Controls';
import Game, { IMapElement, ITile } from './scenes/Game';
import { isIntersect } from '../utils';
import { EPetState } from './Pet';

export enum EDirection {
  LEFT, RIGHT
};

export enum EPlayerLvl {
  STANDART, MEDIUM, PRO
};

export enum EPosition {
  STAND, MOVE, JUMP, DIE, DUCK, SWIM
};

export interface ISprite {
  width: number;
  height: number;
  directions: {sx: number; sy: number;}[][];
}

export default class Player {
  public x: number;
  public y: number;
  public direction: EDirection;
  public isImmortal: boolean;
  protected canvas: HTMLCanvasElement;
  protected ctx: CanvasRenderingContext2D | null;
  protected imgs: IImgs;
  protected speed: number;
  private position: EPosition;
  private scene: Game;
  private controls: IControls;
  private sprites: ISprite[];
  private lastTime: number;
  private currentAnimationFrame: number;
  private jumpInfo: {isJumping: boolean; isFalling: boolean; jumpHeight: number; jumpStartY: number; jumpPressed: boolean;};
  private playerLvl: EPlayerLvl;
  private immortalStartTime: number;
  private tempSprite: {sx: number, sy: number; width: number; height: number} | null;
  private isShowTempSprite: boolean;

  constructor(screen: IScreen, controls: IControls, scene: Game) {
    this.scene = scene;
    this.canvas = screen.canvas;
    this.controls = controls;
    this.ctx = this.canvas.getContext('2d');
    this.imgs = screen.imgs;
    this.direction = EDirection.RIGHT;
    this.position = EPosition.STAND;
    this.speed = 3;
    this.x = 35;
    this.y = 184;
    this.lastTime = 0;
    this.currentAnimationFrame = 0;
    this.jumpInfo = {
      isJumping: false,
      isFalling: false,
      jumpHeight: 69,
      jumpStartY: 0,
      jumpPressed: false,
    };
    this.immortalStartTime = 0;
    this.isImmortal = false;
    this.tempSprite = null;
    this.isShowTempSprite = false;

    this.setPlayerLvl(EPlayerLvl.STANDART);

    // window.setInterval(() => this.setPlayerLvl(Math.floor(Math.random() * 3)), 5000);
  }

  render = (time: number): void => {
    if (!this.ctx) return;
    
    this.update(time);

    const sprite = this.getSprite();;
    const tempSprite = this.tempSprite;;
    if (this.isShowTempSprite && tempSprite) {
      const diffV = tempSprite.height - sprite.height;
      this.ctx.drawImage(this.imgs['characters'], tempSprite.sx, tempSprite.sy, tempSprite.width, tempSprite.height, this.x - this.scene.camera.x, this.y - diffV, tempSprite.width, tempSprite.height);
    } else {
      this.ctx.drawImage(this.imgs['characters'], sprite.sx, sprite.sy, sprite.width, sprite.height, this.x - this.scene.camera.x, this.y, sprite.width, sprite.height);
    }
  }

  private update = (time: number) => {
    this.applyPosition();
    const sprite = this.sprites[this.position];

    if (this.controls.states.up || this.jumpInfo.isJumping || this.jumpInfo.isFalling) {
      this.jump();
    } else {
      this.jumpInfo.jumpPressed = false;
    }

    if (this.controls.states.right) {
      this.goRight();
    } else if (this.controls.states.left) {
      this.goLeft();
    } else if (this.controls.states.down) {
      this.duck()
    } else {
      this.currentAnimationFrame = 0;
    }

    if ((time - this.lastTime) > 150) {
      const { currentAnimationFrame, position, sprites, direction } = this;
      const sprite = sprites[position];
      const maxAnimationFrame = sprite.directions[direction].length - 1;

      this.lastTime = time;
      this.currentAnimationFrame = currentAnimationFrame < maxAnimationFrame ? this.currentAnimationFrame + 1 : 0;
      this.isShowTempSprite = !this.isShowTempSprite;
    }

    if (this.isImmortal) {
      if (!this.immortalStartTime) {
        this.immortalStartTime = time;
      } else if (time - this.immortalStartTime > 1500) {
        this.immortalStartTime = 0;
        this.isImmortal = false;
        this.tempSprite = null;
      }
    }

    const touchedBonus = this.scene.bonuses.find(bonus => {
      const block2 = {x: bonus.getX(), y: bonus.getY(), width: bonus.getWidth(), height: bonus.getHeight()};
      return isIntersect({x: this.x, y: this.y, width: sprite.width, height: sprite.height}, block2);
    });
    if (touchedBonus) {
      this.setPlayerLvl(this.playerLvl === EPlayerLvl.STANDART ? EPlayerLvl.MEDIUM : EPlayerLvl.PRO);
      this.scene.bonuses = this.scene.bonuses.filter(el => el !== touchedBonus);
    }

    const touchedEnemy = this.scene.enemies.find(enemy => {
      const block2 = {x: enemy.getX(), y: enemy.getY(), width: enemy.getWidth(), height: enemy.getHeight()};
      return enemy.getState() === EPetState.DEFAULT && isIntersect({x: this.x, y: this.y, width: sprite.width, height: sprite.height}, block2);
    });
    if (touchedEnemy) {
      if (touchedEnemy.getY() > this.y + sprite.height - this.getJumpSpeed()) {
        this.scene.score += 200;
        this.jumpInfo.isJumping = true;
        this.jumpInfo.jumpStartY = this.y + this.jumpInfo.jumpHeight - 40;

        touchedEnemy.setState(EPetState.DEAD);
      } else {
        if (!this.isImmortal) {
          this.loseLive();
        }
      }
    }
  }

  private applyPosition = () => {
    if (this.jumpInfo.isJumping || this.jumpInfo.isFalling) {
      this.setPosition(EPosition.JUMP);
    } else if (this.controls.states.right || this.controls.states.left) {
      this.setPosition(EPosition.MOVE);
    } else {
      this.setPosition(EPosition.STAND);
    }
  }

  private getSprite = (): {sx: number, sy: number; width: number; height: number} => {
    const { currentAnimationFrame, position, sprites, direction } = this;
    const sprite = sprites[position];
    const spriteCoords = sprite.directions[direction][currentAnimationFrame];

    return {
      sx: spriteCoords.sx,
      sy: spriteCoords.sy,
      width: sprite.width,
      height: sprite.height,
    };
  }

  private goRight = () => {
    if (this.direction !== EDirection.RIGHT) {
      this.currentAnimationFrame = 0;
    }
    this.direction = EDirection.RIGHT;

    const destination = this.x + this.speed;
    if (!this.isBlocking({x: destination, y: this.y})) {
      this.x = destination;
    }
    if (!this.isBlocking({x: this.x, y: this.y + 1}) && !this.jumpInfo.isFalling && !this.jumpInfo.isJumping) {
      this.jumpInfo.isFalling = true;
      this.jumpInfo.jumpStartY = this.y + this.jumpInfo.jumpHeight;
    }
    if (this.x > 3376) this.x = 3376;
  }

  private goLeft = () => {
    if (this.direction !== EDirection.LEFT) {
      this.currentAnimationFrame = 0;
    }
    this.direction = EDirection.LEFT;
    const destination = this.x - this.speed;
    if (!this.isBlocking({x: destination, y: this.y})) {
      this.x = destination;
    }
    if (!this.isBlocking({x: this.x, y: this.y + 1}) && !this.jumpInfo.isFalling && !this.jumpInfo.isJumping) {
      this.jumpInfo.isFalling = true;
      this.jumpInfo.jumpStartY = this.y + this.jumpInfo.jumpHeight;
    }
    if (this.x < this.scene.camera.x) this.x = this.scene.camera.x;
  }

  private duck = () => {
    this.currentAnimationFrame = 0;
    if (this.playerLvl === EPlayerLvl.MEDIUM || this.playerLvl === EPlayerLvl.PRO) {
      this.setPosition(EPosition.DUCK);
    }
  }

  private getJumpSpeed = (): number => {
    const { jumpHeight, jumpStartY } = this.jumpInfo;
    const passedRange = jumpStartY - this.y;
    return 11.5 - Math.pow(512 * (passedRange > 0 ? passedRange : 0) / jumpHeight, 1/3);
  }

  private jump = () => {
    this.currentAnimationFrame = 0;
    const sprite = this.getSprite();
    const { isJumping, isFalling, jumpHeight, jumpStartY } = this.jumpInfo;

    if (!isJumping && !isFalling) {
      if (!this.jumpInfo.jumpPressed) {
        this.jumpInfo.isJumping = true;
        this.jumpInfo.jumpStartY = this.y;
        this.jumpInfo.jumpPressed = true;
      }
    } else {
      const jumpSpeed = this.getJumpSpeed();
      if (isJumping) {
        const destination = this.y - jumpSpeed;
        const blockingElement = this.isBlocking({x: this.x, y: destination});
        if (!blockingElement) {
          this.y = destination;
        } else {
          // Марио задел бошкой препятствие
          this.scene.tryDestroyBlock(blockingElement);
          this.y = blockingElement.y + blockingElement.height;
          this.jumpInfo.isJumping = false;
          this.jumpInfo.isFalling = true;
        }
        if (!this.controls.states.up && jumpStartY - this.y > 55) { // Отпустили кнопку вверх и набрали немного высоты
          this.jumpInfo.isJumping = false;
          this.jumpInfo.isFalling = true;
        }
        if (this.y < jumpStartY - jumpHeight) { // Достигнута макс высота
          this.jumpInfo.isJumping = false;
          this.jumpInfo.isFalling = true;
        }
      } else {
        const destination = this.y + jumpSpeed;
        const blockingElement = this.isBlocking({x: this.x, y: destination});
        if (!blockingElement) {
          if (destination < 400) {
            this.y = destination;
            return;
          }
          this.y = 400;
          this.scene.isGameOver = true;
        } else {
          this.y = blockingElement.y - sprite.height;
        }
        this.jumpInfo.isFalling = false;
        this.jumpInfo.isJumping = false;
        this.jumpInfo.jumpStartY = 0;
      }
    }
  }

  private isBlocking = (point: {x: number; y: number;}): (IMapElement & ITile) | undefined => {
    const { width: playerWidth, height: playerHeight } = this.sprites[this.position];
    const tempMap = this.scene.getMergedMap();

    const block = tempMap.find(el => {
      const block1 = {x: point.x + 1, y: point.y, width: playerWidth - 2, height: playerHeight}; // Уменьшим ширину ГГ, иначе он не помещается между блоков
      const block2 = {x: el.x, y: el.y, width: el.width, height: el.height};

      return !el.passable && isIntersect(block1, block2);
    });

    return block;
  }

  public getPlayerLvl = () => {
    return this.playerLvl;
  }

  public setPlayerLvl = (playerLvl: EPlayerLvl) => {
    this.playerLvl = playerLvl;
    this.currentAnimationFrame = 0;
    this.isImmortal = true;
    const allSprites = [
      [
        {width: 16, height: 16, directions: [[{sx: 223, sy: 44}], [{sx: 276, sy: 44}],]},
        {width: 16, height: 16, directions: [[{sx: 206, sy: 44}, {sx: 193, sy: 44}, {sx: 176, sy: 44}], [{sx: 291, sy: 44}, {sx: 305, sy: 44}, {sx: 321, sy: 44}],]},
        {width: 16, height: 16, directions: [[{sx: 142, sy: 44}], [{sx: 355, sy: 44}],]},
        {width: 14, height: 14, directions: [[{sx: 13, sy: 46}], [{sx: 486, sy: 46}],]},
        {width: 12, height: 16, directions: [[{sx: 224, sy: 44}], [{sx: 277, sy: 44}],]},
        {width: 14, height: 15, directions: [
          [{sx: 94, sy: 45}, {sx: 78, sy: 45}, {sx: 62, sy: 45}, {sx: 46, sy: 45}, {sx: 30, sy: 45}],
          [{sx: 405, sy: 45}, {sx: 421, sy: 45}, {sx: 437, sy: 45}, {sx: 453, sy: 45}, {sx: 469, sy: 45}],
        ]},
      ], [
        {width: 16, height: 32, directions: [[{sx: 239, sy: 1}], [{sx: 258, sy: 1}],]},
        {width: 16, height: 32, directions: [[{sx: 201, sy: 1}, {sx: 183, sy: 1}, {sx: 166, sy: 1}], [{sx: 296, sy: 1}, {sx: 314, sy: 1}, {sx: 331, sy: 1}],]},
        {width: 16, height: 32, directions: [[{sx: 128, sy: 1}], [{sx: 369, sy: 1}],]},
        {width: 14, height: 14, directions: [[{sx: 13, sy: 46}], [{sx: 486, sy: 46}],]},
        {width: 16, height: 22, directions: [[{sx: 220, sy: 11}], [{sx: 277, sy: 11}],]},
        {width: 16, height: 30, directions: [
          [{sx: 75, sy: 3}, {sx: 56, sy: 3}, {sx: 38, sy: 3}, {sx: 20, sy: 3}, {sx: 1, sy: 3},],
          [{sx: 422, sy: 3}, {sx: 441, sy: 3}, {sx: 459, sy: 3}, {sx: 477, sy: 3}, {sx: 496, sy: 3},],
        ]},
      ], [
        {width: 16, height: 32, directions: [[{sx: 239, sy: 125}], [{sx: 258, sy: 125}],]},
        {width: 16, height: 32, directions: [[{sx: 201, sy: 125}, {sx: 183, sy: 125}, {sx: 166, sy: 125}], [{sx: 296, sy: 125}, {sx: 314, sy: 125}, {sx: 331, sy: 125}],]},
        {width: 16, height: 32, directions: [[{sx: 128, sy: 125}], [{sx: 369, sy: 125}],]},
        {width: 14, height: 14, directions: [[{sx: 13, sy: 46}], [{sx: 486, sy: 46}],]},
        {width: 16, height: 22, directions: [[{sx: 220, sy: 135}], [{sx: 277, sy: 135}],]},
        {width: 16, height: 30, directions: [
          [{sx: 75, sy: 127}, {sx: 56, sy: 127}, {sx: 38, sy: 127}, {sx: 20, sy: 127}, {sx: 1, sy: 127},],
          [{sx: 422, sy: 127}, {sx: 441, sy: 127}, {sx: 459, sy: 127}, {sx: 477, sy: 127}, {sx: 496, sy: 127},],
        ]},
      ],
    ];

    const featureSprite = allSprites[playerLvl];
    if (this.sprites && this.sprites.length) {
      const currentSprite = this.getSprite();

      if (currentSprite.height !== featureSprite[this.position].height) {
        this.y += currentSprite.height - featureSprite[this.position].height;
      }

      this.tempSprite = currentSprite;
    }

    this.sprites = featureSprite;
  }

  public setPosition = (position: EPosition) => {
    if (this.sprites && this.sprites.length) {
      const currentSprite = this.getSprite();
      const featureSprite = this.sprites[position];
      if (currentSprite.height !== featureSprite.height) {
        this.y += currentSprite.height - featureSprite.height;
      }
    }
    this.position = position;
  }

  public loseLive = () => {
    if (this.playerLvl !== EPlayerLvl.STANDART) {
      this.setPlayerLvl(this.playerLvl === EPlayerLvl.MEDIUM ? EPlayerLvl.STANDART : EPlayerLvl.MEDIUM);
    }

    this.isImmortal = true;

    this.scene.lives--;
    if (this.scene.lives < 1) {
      this.scene.isGameOver = true;
    }
  }

  public reset = () => {
    this.setPlayerLvl(EPlayerLvl.STANDART);
    this.direction = EDirection.RIGHT;
    this.setPosition(EPosition.STAND);
    this.x = 35;
    this.y = 184;
    this.jumpInfo = {
      ...this.jumpInfo,
      isFalling: false,
      isJumping: false,
      jumpPressed: false,
      jumpStartY: 0,
    },
    this.isImmortal = false;
    this.immortalStartTime = 0;
    this.isShowTempSprite = false;
    this.tempSprite = null;
  }
}