export type TCallback = (time: number) => void;

export default class GameLoop {
  private lastTime: number;
  private callback: TCallback;

  constructor() {
    this.lastTime = 0;
  }

  start = (callback: TCallback) => {
    this.callback = callback;
    requestAnimationFrame(this.frame);
  }

  frame = (time: number) => {
    if ((time - this.lastTime) > 30) {
      this.lastTime = time;
      this.callback(time);
    }

    requestAnimationFrame(this.frame);
  }
}