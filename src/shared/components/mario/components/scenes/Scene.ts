import { IScreen, IImgs, TSceneName } from '../../index';
import { IControls } from '../Controls';

export interface IScene {
  render: (time: number) => TSceneName | null;
};

export default class Scene implements IScene {
  protected canvas: HTMLCanvasElement;
  protected ctx: CanvasRenderingContext2D | null;
  protected controls: IControls;
  protected imgs: IImgs;
  protected zoom: number;

  constructor(screen: IScreen, controls: IControls) {
    this.canvas = screen.canvas;
    this.ctx = this.canvas.getContext('2d');

    this.controls = controls;
    this.imgs = screen.imgs;
  }

  render = (time: number) : TSceneName | null => {
    return null;
  }
} 