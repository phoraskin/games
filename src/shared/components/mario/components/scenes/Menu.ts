import { IScreen, TSceneName } from '../../index';
import { IControls } from '../Controls';
import Scene from './Scene';

export default class Menu extends Scene {
  constructor(screen: IScreen, controls: IControls) {
    super(screen, controls);
  }

  render = (time: number): TSceneName | null => {
    if (!this.ctx) return null;

    this.ctx.drawImage(this.imgs['title'], 0, 0, this.canvas.width / 1.5, this.canvas.height / 1.5);

    this.ctx.fillStyle = '#ffffff';
    this.ctx.font = '22px Georgia';
    this.ctx.fillText('Press space to start game', this.canvas.width / 2 - 250, this.canvas.height / 2 - 45);

    if (this.controls.states['fire']) {
      return 'game';
    }

    return 'menu';
  }
}