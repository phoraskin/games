import { IScreen, TSceneName } from '../../index';
import { IControls } from '../Controls';
import Scene from './Scene';

export type TStatus = 'loading' | 'loaded';

export default class Lib extends Scene {
  private assets: {name: string, path: string}[];
  private loaded: number;
  private status: TStatus;
  private loadedAt: number;

  constructor(screen: IScreen, controls: IControls) {
    super(screen, controls);

    this.assets = [
      {name: 'characters', path: 'assets/mario/characters.png'},
      {name: 'tiles', path: 'assets/mario/tiles.png'},
      {name: 'title', path: 'assets/mario/title.jpg'},
      {name: 'win', path: 'assets/mario/win.jpg'},
    ];
    this.loaded = 0;
    this.status = 'loading'
    this.loadedAt = 0;

    for(const asset of this.assets) {
      const img = new Image();
      img.onload = () => {
        this.loaded++;
      }

      img.src = asset.path;
      screen.imgs[asset.name] = img;
    }
  }

  render = (time: number): TSceneName | null => {
    if (!this.ctx) return null;

    if (this.status === 'loading') {
      if (this.loaded === this.assets.length) {
        this.status = 'loaded';
        this.loadedAt = time;
      }

      this.ctx.fillStyle = '#000000';
			this.ctx.fillRect(0, 0, this.canvas.width, this.canvas.height );
			this.ctx.fillStyle = '#ffffff';
			this.ctx.font = '22px Georgia';
      // this.ctx.fillText(`Loading ${this.loaded}/${this.assets.length}`, 200, this.canvas.height / 2 - 20);
      this.ctx.fillText(`Loading ${this.loaded * 100 / this.assets.length}%`, 200, this.canvas.height / 2 - 20);

			return 'lib';
    }

    if (this.status === 'loaded') {
			if ((time - this.loadedAt) > 1000) {
				return 'menu';
			} else {
				return 'lib';
			}
    }
    
    return null;
  }
}