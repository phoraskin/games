import { IScreen, TSceneName } from '../../index';
import { IControls } from '../Controls';
import Scene from './Scene';
import Player, { EPlayerLvl, EDirection } from '../Player';
import Camera from '../Camera';
import Fireball from '../Fireball';
import { debounce } from '../../utils';
import Pet, { IPet, EPetState } from '../Pet';
import Mushroom from '../enemies/Mushroom';
import Turtle from '../enemies/Turtle';
import BonusMushroom from '../bonuses/Mushroom';
import BonusStar from '../bonuses/Star';
import BonusFlower from '../bonuses/Flower';

export enum EBonus {
  COIN, LVL_UP, GOD_MOD
};

export enum EEnemyType {
  MUSHROOM, TURTLE
}

export enum EBonusType {
  MUSHROOM, STAR, FLOWER
}

export interface ITile {
  sx: number;
  sy: number;
  width: number;
  height: number;
  passable: boolean;
};

export enum ETile {
  GROUND, GROUND_REVERSE, WALL, WALL_REVERSE, BONUS, BONUS_USED, WALL_UNBREAKABLE, PIPE_SMALL, PIPE_MEDIUM, PIPE_BIG, COIN, CASTLE, FLAG, HILL_BIG, HILL_MEDIUM, SHRUB_BIG, SHRUB_MEDIUM, SHRUB_SMALL, CLOUD_BIG, CLOUD_MEDIUM, CLOUD_SMALL
};

export interface IBonus {
  type: EBonus;
};

export interface ICoinBonus extends IBonus {
  coins: number;
};

export interface IMapElement {
  tile: ETile;
  x: number;
  y: number;
  bonus?: IBonus | ICoinBonus;
};

export type IMergedMapElement = IMapElement & ITile;

export enum EAnimation {
  SHAKE, DESTROY
};

export default class Game extends Scene {
  public player: Player;
  public camera: Camera;
  public isGameOver: boolean;
  public enemies: IPet[];
  public bonuses: IPet[];
  public score: number;
  public coins: number;
  public lives: number;
  private map: IMapElement[];
  private mergedMap: IMergedMapElement[];
  private tiles: ITile[];
  private remaining: number
  private lastRemainingTime: number;
  private fireballs: Fireball[];
  private animateBlocks: {element: IMergedMapElement, progress: number, type: EAnimation, callback?: () => void}[];
  private lastFilterEnemiesTime: number;

  constructor(screen: IScreen, controls: IControls) {
    super(screen, controls);

    this.tiles = [
      {sx: 34, sy: 33, width: 16, height: 16, passable: false}, // ground standart
      {sx: 17, sy: 50, width: 16, height: 16, passable: false}, // ground reverse
      {sx: 34, sy: 50, width: 16, height: 16, passable: false}, // wall standart breakable
      {sx: 0, sy: 50, width: 16, height: 16, passable: false}, // wall reverse breakable
      {sx: 51, sy: 33, width: 16, height: 16, passable: false}, // bonus block
      {sx: 81, sy: 80, width: 16, height: 16, passable: false}, // bonus used
      {sx: 51, sy: 50, width: 16, height: 16, passable: false}, // wall unbreakable
      {sx: 34, sy: 0, width: 32, height: 32, passable: false}, // pipe small
      {sx: 0, sy: 0, width: 32, height: 48, passable: false}, // pipe medium
      {sx: 67, sy: 0, width: 32, height: 64, passable: false}, // pipe tall
      {sx: 78, sy: 65, width: 16, height: 14, passable: true}, // coin
      {sx: 0, sy: 67, width: 80, height: 80, passable: true}, // castle
      {sx: 100, sy: 0, width: 20, height: 152, passable: true}, // flag
      {sx: 0, sy: 148, width: 80, height: 35, passable: true}, // hill big
      {sx: 65, sy: 184, width: 48, height: 19, passable: true}, // hill medium
      {sx: 0, sy: 184, width: 64, height: 16, passable: true}, // shrub big
      {sx: 33, sy: 229, width: 48, height: 16, passable: true}, // shrub medium
      {sx: 81, sy: 167, width: 32, height: 16, passable: true}, // shrub small
      {sx: 0, sy: 204, width: 64, height: 24, passable: true}, // cloud big
      {sx: 65, sy: 204, width: 48, height: 24, passable: true}, // cloud medium
      {sx: 0, sy: 229, width: 32, height: 24, passable: true}, // cloud small
    ];

    this.generateMap();

    this.generateMergedMap();

    this.player = new Player(screen, controls, this);
    this.camera = new Camera(0, 0, this);
    this.isGameOver = false;
    this.score = 0;
    this.coins = 0;
    this.lives = 3;
    this.remaining = 400;
    this.lastRemainingTime = 0;
    this.fireballs = [];
    this.makeFirebal = debounce(this.makeFirebal, 2000);
    this.animateBlocks = [];
    this.lastFilterEnemiesTime = 0;

    this.generateEnemies();
    this.bonuses = [];
  }

  public render = (time: number): TSceneName | null => {
    if (!this.ctx) return null;

    if (time - this.lastRemainingTime > 500) {
      this.lastRemainingTime = time;
      this.remaining--;
      if (this.remaining < 0) {
        this.isGameOver = true;
        this.remaining = 0;
      }
    }

    if (time - this.lastFilterEnemiesTime > 1500) {
      this.lastFilterEnemiesTime = time;
      this.enemies = this.enemies.filter(el => el.getState() !== EPetState.DEAD);
    }

    this.handleControls();

    this.ctx.fillStyle = '#2f94fe';
    this.ctx.fillRect(0, 0, this.canvas.width, this.canvas.height);

    for (let mapElement of this.map) {
      const tile = this.tiles[mapElement.tile];
      this.ctx.drawImage(this.imgs['tiles'], tile.sx, tile.sy, tile.width, tile.height, mapElement.x - this.camera.x, mapElement.y, tile.width, tile.height);
    }

    this.animateBlocks.forEach(animateBlock => {
      if (!this.ctx) return;
      const { sx, sy, width, height, x, y } = animateBlock.element;
      if (animateBlock.progress < 100) {
        animateBlock.progress += 10;
        if (animateBlock.type === EAnimation.SHAKE) {
          const offsetY = animateBlock.progress <= 50 ? -0.2 * animateBlock.progress : -0.2 * (100 - animateBlock.progress);
          this.ctx.drawImage(this.imgs['tiles'], sx, sy, width, height, x - this.camera.x, y + offsetY, width, height);
        }
        if (animateBlock.type === EAnimation.DESTROY) {
          const halfW = width / 2;
          const halfH = height / 2;
          const offsetX = 0.2 * animateBlock.progress;
          const offsetY = animateBlock.progress <= 50 ? 0.2 * animateBlock.progress : 0.2 * (100 - animateBlock.progress);
          this.ctx.drawImage(this.imgs['tiles'], sx, sy, halfW, halfH, x - this.camera.x - offsetX, y - offsetY - height, halfW, halfH);
          this.ctx.drawImage(this.imgs['tiles'], sx + halfW, sy, halfW, halfH, x - this.camera.x + halfW + offsetX, y - offsetY - height, halfW, halfH);
          this.ctx.drawImage(this.imgs['tiles'], sx, sy + halfH, halfW, halfH, x - this.camera.x - offsetX, y - offsetY + 20 - height, halfW, halfH);
          this.ctx.drawImage(this.imgs['tiles'], sx + halfW, sy + halfH, halfW, halfH, x - this.camera.x + halfW + offsetX, y - offsetY + 20 - height, halfW, halfH);
        }
      } else {
        this.animateBlocks = this.animateBlocks.filter(el => el !== animateBlock);
        animateBlock.callback && animateBlock.callback();
      }
    });

    this.camera.update(time);
    this.player.render(time);
    this.fireballs.forEach(fireball => {
      if (!fireball) return;

      fireball.update(time, this.camera.x);
      if (this.ctx) {
        fireball.render(this.ctx);
      }
    });

    for (const enemy of this.enemies) {
      if (enemy.getX() - this.camera.x < -enemy.getWidth() || enemy.getX() - this.camera.x > this.canvas.width / 1.5) {
        continue;
      }
      enemy.update(time, this.camera.x).render(this.ctx);
    };

    for (const bonus of this.bonuses) {
      if (bonus.getX() - this.camera.x < -bonus.getWidth() || bonus.getX() - this.camera.x > this.canvas.width / 1.5) {
        continue;
      }
      bonus.update(time, this.camera.x).render(this.ctx);
    };

    this.ctx.fillStyle = '#ffffff';
    this.ctx.font = 'bold 12px Arial';
    this.ctx.fillText('SCORE', 20, 10);
    this.ctx.fillText(`0000000${this.score}`.slice(-7), 18, 20);
    this.ctx.fillText('COINS', 150, 10);
    this.ctx.fillText(`00000${this.coins}`.slice(-5), 152, 20);
    this.ctx.fillText('WORLD', this.canvas.width / 3 - 5, 10);
    this.ctx.fillText('1-1', this.canvas.width / 3 + 7, 20);
    this.ctx.fillText('TIME', this.canvas.width / 3 + 100, 10);
    this.ctx.fillText(`000${this.remaining}`.slice(-3), this.canvas.width / 3 + 105, 20);
    this.ctx.fillText('LIVES', this.canvas.width / 1.5 - 50, 10);
    this.ctx.fillText(this.lives.toString(), this.canvas.width / 1.5 - 35, 20);

    if (this.player.x > 3177) {
      this.resetGame();

      return 'gameWin';
    }

    if (this.isGameOver) {
      this.resetGame();

      return 'gameOver';
    }

    return 'game';
  }

  public getMap = () => {
    return this.map;
  }

  public getTiles = () => {
    return this.tiles;
  }

  public getMergedMap = () => {
    return this.mergedMap;
  }

  private isCoinBonusType = (bonus?: IBonus | ICoinBonus): bonus is ICoinBonus => {
    return !!(bonus && bonus.type === EBonus.COIN && (bonus as ICoinBonus).coins);
  }

  public tryDestroyBlock = (element: IMergedMapElement) => {
    const playerLvl = this.player.getPlayerLvl();
    const cloneElement = {...element};
    let shouldBreak: boolean = false;

    switch (element.tile) {
      case ETile.BONUS:
        shouldBreak = true;
        this.score += 200;
        if (this.isCoinBonusType(element.bonus) && element.bonus.coins) {
          element.bonus.coins -= 100;
          this.coins ++;
          const coinTile = this.tiles[ETile.COIN];
          const coinElement: IMergedMapElement = {tile: ETile.COIN, x: element.x, y: element.y - element.height, width: coinTile.width, height: coinTile.height, sx: coinTile.sx, sy: coinTile.sy, passable: coinTile.passable};
          this.animateBlocks.push({element: coinElement, progress: 0, type: EAnimation.SHAKE});
        }
        this.animateBlocks.push({element, progress: 0, type: EAnimation.SHAKE, callback: () => {
          if (!this.isCoinBonusType(element.bonus) || element.bonus.coins <= 0) {
            cloneElement.tile = ETile.BONUS_USED;
          }
          this.map.push(cloneElement);
          this.generateMergedMap();
          if (element.bonus && element.bonus.type === EBonus.LVL_UP) {
            this.makeBonus(this.player.getPlayerLvl() === EPlayerLvl.STANDART ? EBonusType.MUSHROOM : EBonusType.FLOWER, element.x, element.y - element.height);
          }
        }});
        break;
      case ETile.WALL:
        shouldBreak = true;
        if (playerLvl === EPlayerLvl.STANDART) {
          this.animateBlocks.push({element, progress: 0, type: EAnimation.SHAKE, callback: () => {
            this.map.push(cloneElement);
            this.generateMergedMap();
          }});
        } else {
          this.animateBlocks.push({element, progress: 0, type: EAnimation.DESTROY});
        }
        break;
    }
    
    if (shouldBreak) {
      this.map = this.map.filter(el => {
        return el.x !== element.x || el.y !== element.y || el.tile !== element.tile;
      });
      this.generateMergedMap();
    }
  }

  private generateMap = () => {
    this.map = [
      {tile: ETile.HILL_BIG, x: 0, y: 165},
      {tile: ETile.SHRUB_BIG, x: 184, y: 184},
      {tile: ETile.HILL_MEDIUM, x: 256, y: 181},
      {tile: ETile.SHRUB_SMALL, x: 376, y: 184},
      {tile: ETile.PIPE_SMALL, x: 448, y: 168},
      {tile: ETile.PIPE_MEDIUM, x: 608, y: 152},
      {tile: ETile.SHRUB_MEDIUM, x: 664, y: 184},
      {tile: ETile.PIPE_BIG, x: 736, y: 136},
      {tile: ETile.HILL_BIG, x: 768, y: 165},
      {tile: ETile.PIPE_BIG, x: 912, y: 136},
      {tile: ETile.SHRUB_BIG, x: 952, y: 184},
      {tile: ETile.HILL_MEDIUM, x: 1024, y: 181},
      {tile: ETile.SHRUB_SMALL, x: 1144, y: 184},
      {tile: ETile.SHRUB_MEDIUM, x: 1432, y: 184},
      {tile: ETile.HILL_BIG, x: 1536, y: 165},
      {tile: ETile.SHRUB_BIG, x: 1720, y: 184},
      {tile: ETile.HILL_MEDIUM, x: 1791, y: 181},
      {tile: ETile.SHRUB_SMALL, x: 1912, y: 184},
      {tile: ETile.SHRUB_BIG, x: 2181, y: 184},
      ...this.generateBlockLadder(2144, 136),
      {tile: ETile.HILL_BIG, x: 2304, y: 165},
      ...this.generateBlockLadder(2384, 136),
      {tile: ETile.HILL_MEDIUM, x: 2560, y: 181},
      {tile: ETile.PIPE_SMALL, x: 2608, y: 168},
      {tile: ETile.SHRUB_SMALL, x: 2680, y: 184},
      {tile: ETile.PIPE_SMALL, x: 2862, y: 168},
      ...this.generateFinishBlockLadder(2896, 72),
      {tile: ETile.HILL_BIG, x: 3072, y: 165},
      {tile: ETile.WALL_UNBREAKABLE, x: 3168, y: 184},
      {tile: ETile.FLAG, x: 3160, y: 32},
      {tile: ETile.CASTLE, x: 3232, y: 120},
      {tile: ETile.HILL_MEDIUM, x: 3328, y: 181},
      ...this.generateGround(),
      {tile: ETile.BONUS, x: 256, y: 136, bonus: {type: EBonus.COIN, coins: 100}},
      {tile: ETile.WALL, x: 320, y: 136},
      {tile: ETile.BONUS, x: 336, y: 136, bonus: {type: EBonus.LVL_UP}},
      {tile: ETile.WALL, x: 352, y: 136},
      {tile: ETile.BONUS, x: 368, y: 136, bonus: {type: EBonus.COIN, coins: 100}},
      {tile: ETile.WALL, x: 384, y: 136},
      {tile: ETile.BONUS, x: 352, y: 72, bonus: {type: EBonus.COIN, coins: 100}},
      {tile: ETile.CLOUD_SMALL, x: 312, y: 24},
      {tile: ETile.CLOUD_BIG, x: 440, y: 40},
      {tile: ETile.CLOUD_MEDIUM, x: 584, y: 24},
      {tile: ETile.CLOUD_SMALL, x: 904, y: 40},
      {tile: ETile.CLOUD_SMALL, x: 1080, y: 24},
      {tile: ETile.CLOUD_BIG, x: 1208, y: 40},
      {tile: ETile.WALL, x: 1232, y: 136},
      {tile: ETile.BONUS, x: 1248, y: 136, bonus: {type: EBonus.LVL_UP}},
      {tile: ETile.WALL, x: 1264, y: 136},
      {tile: ETile.WALL, x: 1280, y: 72},
      {tile: ETile.WALL, x: 1296, y: 72},
      {tile: ETile.WALL, x: 1312, y: 72},
      {tile: ETile.WALL, x: 1328, y: 72},
      {tile: ETile.WALL, x: 1344, y: 72},
      {tile: ETile.WALL, x: 1360, y: 72},
      {tile: ETile.WALL, x: 1376, y: 72},
      {tile: ETile.WALL, x: 1392, y: 72},
      {tile: ETile.WALL, x: 1456, y: 72},
      {tile: ETile.WALL, x: 1472, y: 72},
      {tile: ETile.WALL, x: 1488, y: 72},
      {tile: ETile.BONUS, x: 1504, y: 72, bonus: {type: EBonus.COIN, coins: 100}},
      {tile: ETile.WALL, x: 1504, y: 136},
      {tile: ETile.WALL, x: 1600, y: 136},
      {tile: ETile.BONUS, x: 1696, y: 136, bonus: {type: EBonus.COIN, coins: 100}},
      {tile: ETile.BONUS, x: 1744, y: 136, bonus: {type: EBonus.COIN, coins: 100}},
      {tile: ETile.BONUS, x: 1792, y: 136, bonus: {type: EBonus.COIN, coins: 100}},
      {tile: ETile.BONUS, x: 1744, y: 72, bonus: {type: EBonus.LVL_UP}},
      {tile: ETile.CLOUD_SMALL, x: 1672, y: 40},
      {tile: ETile.CLOUD_SMALL, x: 1848, y: 24},
      {tile: ETile.CLOUD_BIG, x: 1976, y: 40},
      {tile: ETile.WALL, x: 1936, y: 72},
      {tile: ETile.WALL, x: 1952, y: 72},
      {tile: ETile.WALL, x: 1968, y: 72},
      {tile: ETile.WALL, x: 2048, y: 72},
      {tile: ETile.BONUS, x: 2064, y: 72, bonus: {type: EBonus.COIN, coins: 100}},
      {tile: ETile.BONUS, x: 2080, y: 72, bonus: {type: EBonus.COIN, coins: 100}},
      {tile: ETile.WALL, x: 2096, y: 72},
      {tile: ETile.WALL, x: 2064, y: 136},
      {tile: ETile.WALL, x: 2080, y: 136},
      {tile: ETile.CLOUD_MEDIUM, x: 2120, y: 24},
      {tile: ETile.CLOUD_SMALL, x: 2440, y: 40},
      {tile: ETile.CLOUD_SMALL, x: 2616, y: 24},
      {tile: ETile.CLOUD_BIG, x: 2744, y: 40},
      {tile: ETile.WALL, x: 2688, y: 136},
      {tile: ETile.WALL, x: 2704, y: 136},
      {tile: ETile.BONUS, x: 2720, y: 136, bonus: {type: EBonus.LVL_UP}},
      {tile: ETile.WALL, x: 2736, y: 136},
      {tile: ETile.CLOUD_MEDIUM, x: 2888, y: 24},
      {tile: ETile.CLOUD_SMALL, x: 3208, y: 40},
    ];
  }

  private generateMergedMap = () => {
    this.mergedMap = this.map.map(el => {
      return {...el, ...this.tiles[el.tile]};
    });
  }

  private generateBlockLadder = (startX: number, startY: number): IMapElement[] => {
    return Array(20).fill(null).map<IMapElement>((el, index) => {
      let x, y;
      if (index < 4) {x = startX + index * this.tiles[ETile.WALL_UNBREAKABLE].width; y = startY + 48;}
      else if (index < 7) {x = startX + (index - 3) * this.tiles[ETile.WALL_UNBREAKABLE].width; y = startY + 32;}
      else if (index < 9) {x = startX + (index - 5) * this.tiles[ETile.WALL_UNBREAKABLE].width; y = startY + 16;}
      else if (index === 9) {x = startX + 3 * this.tiles[ETile.WALL_UNBREAKABLE].width; y = startY;}
      else if (index === 10) {x = startX + 6 * this.tiles[ETile.WALL_UNBREAKABLE].width; y = startY;}
      else if (index < 13) {x = startX + (index - 5) * this.tiles[ETile.WALL_UNBREAKABLE].width; y = startY + 16;}
      else if (index < 16) {x = startX + (index - 7) * this.tiles[ETile.WALL_UNBREAKABLE].width; y = startY + 32;}
      else {x = startX + (index - 10) * this.tiles[ETile.WALL_UNBREAKABLE].width; y = startY + 48;}

      return {tile: ETile.WALL_UNBREAKABLE, x, y};
    });
  }

  private generateFinishBlockLadder = (startX: number, startY: number): IMapElement[] => {
    return Array(44).fill(null).map<IMapElement>((el, index) => {
      let x, y;
      if (index < 9) {x = startX + index * this.tiles[ETile.WALL_UNBREAKABLE].width; y = startY + 112;}
      else if (index < 17) {x = startX + (index - 8) * this.tiles[ETile.WALL_UNBREAKABLE].width; y = startY + 96;}
      else if (index < 24) {x = startX + (index - 15) * this.tiles[ETile.WALL_UNBREAKABLE].width; y = startY + 80;}
      else if (index < 30) {x = startX + (index - 21) * this.tiles[ETile.WALL_UNBREAKABLE].width; y = startY + 64;}
      else if (index < 35) {x = startX + (index - 26) * this.tiles[ETile.WALL_UNBREAKABLE].width; y = startY + 48;}
      else if (index < 39) {x = startX + (index - 30) * this.tiles[ETile.WALL_UNBREAKABLE].width; y = startY + 32;}
      else if (index < 42) {x = startX + (index - 33) * this.tiles[ETile.WALL_UNBREAKABLE].width; y = startY + 16;}
      else {x = startX + (index - 35) * this.tiles[ETile.WALL_UNBREAKABLE].width; y = startY;}

      return {tile: ETile.WALL_UNBREAKABLE, x, y};
    });
  }

  private generateGround = (): IMapElement[] => {
    let map: IMapElement[] = [];
    for (let i = 0; i < 69; i++) {
      map.push(
        {tile: ETile.GROUND, x: (i * this.tiles[ETile.WALL_UNBREAKABLE].width), y: 200},
        {tile: ETile.GROUND, x: (i * this.tiles[ETile.WALL_UNBREAKABLE].width), y: 216},
      );
    }
    for (let i = 0; i < 15; i++) {
      map.push(
        {tile: ETile.GROUND, x: 1136 + (i * this.tiles[ETile.WALL_UNBREAKABLE].width), y: 200},
        {tile: ETile.GROUND, x: 1136 + (i * this.tiles[ETile.WALL_UNBREAKABLE].width), y: 216},
      );
    }
    for (let i = 0; i < 64; i++) {
      map.push(
        {tile: ETile.GROUND, x: 1424 + (i * this.tiles[ETile.WALL_UNBREAKABLE].width), y: 200},
        {tile: ETile.GROUND, x: 1424 + (i * this.tiles[ETile.WALL_UNBREAKABLE].width), y: 216},
      );
    }
    for (let i = 0; i < 57; i++) {
      map.push(
        {tile: ETile.GROUND, x: 2480 + (i * this.tiles[ETile.WALL_UNBREAKABLE].width), y: 200},
        {tile: ETile.GROUND, x: 2480 + (i * this.tiles[ETile.WALL_UNBREAKABLE].width), y: 216},
      );
    }

    return map;
  }

  private handleControls = () => {
    if (this.controls.states['fire'] && this.player.getPlayerLvl() !== EPlayerLvl.STANDART) {
      const { x: playerX, y: playerY, direction } = this.player;
      this.makeFirebal(playerX, playerY + 10, direction);
    }
  }

  private makeFirebal = (x: number, y: number, direction: EDirection) => {
    const fireball = new Fireball(x, y, direction);
    fireball.setCanvas(this.canvas);
    fireball.setSelfDestroy(() => {this.fireballs = this.fireballs.filter(el => el !== fireball)});
    fireball.setBarriers(this.mergedMap.filter(el => !el.passable).map(el => ({x: el.x, y: el.y, width: el.width, height: el.height})));
    this.fireballs.push(fireball);
  }

  private makeEnemy = (enemyType: EEnemyType, x: number, y: number) => {
    const barriers = this.mergedMap.filter(el => !el.passable).map(el => ({x: el.x, y: el.y, width: el.width, height: el.height}));
    var enemy;
    switch (enemyType) {
      case EEnemyType.MUSHROOM:
        enemy = new Mushroom(x, y, barriers, this.imgs);
        break;
      case EEnemyType.TURTLE:
        enemy = new Turtle(x, y, barriers, this.imgs);
        break;
      default:
        enemy = new Pet(x, y, barriers, this.imgs);
        break;
    }
    
    this.enemies.push(enemy)
  }

  private generateEnemies = () => {
    this.enemies = [];
    this.makeEnemy(EEnemyType.MUSHROOM, 365, 184);
    this.makeEnemy(EEnemyType.MUSHROOM, 680, 184);
    this.makeEnemy(EEnemyType.MUSHROOM, 820, 184);
    this.makeEnemy(EEnemyType.MUSHROOM, 840, 184);
    this.makeEnemy(EEnemyType.MUSHROOM, 1305, 56);
    this.makeEnemy(EEnemyType.MUSHROOM, 1325, 56);
    this.makeEnemy(EEnemyType.MUSHROOM, 1500, 184);
    this.makeEnemy(EEnemyType.MUSHROOM, 1520, 184);
    this.makeEnemy(EEnemyType.TURTLE, 1650, 176);
    this.makeEnemy(EEnemyType.MUSHROOM, 1830, 184);
    this.makeEnemy(EEnemyType.MUSHROOM, 1850, 184);
    this.makeEnemy(EEnemyType.MUSHROOM, 1900, 184);
    this.makeEnemy(EEnemyType.MUSHROOM, 1925, 184);
    this.makeEnemy(EEnemyType.MUSHROOM, 2000, 184);
    this.makeEnemy(EEnemyType.MUSHROOM, 2100, 184);
    this.makeEnemy(EEnemyType.MUSHROOM, 2715, 184);
    this.makeEnemy(EEnemyType.MUSHROOM, 2735, 184);
  }

  private makeBonus = (bonusType: EBonusType, x: number, y: number) => {
    const barriers = this.mergedMap.filter(el => !el.passable).map(el => ({x: el.x, y: el.y, width: el.width, height: el.height}));
    var bonus;
    switch (bonusType) {
      case EBonusType.MUSHROOM:
        bonus = new BonusMushroom(x, y, barriers, this.imgs);
        break;
      case EBonusType.STAR:
        bonus = new BonusStar(x, y, barriers, this.imgs);
        break;
      case EBonusType.FLOWER:
        bonus = new BonusFlower(x, y, barriers, this.imgs);
        break;
      default:
        bonus = new Pet(x, y, barriers, this.imgs);
        break;
    }
    bonus.setDirection(EDirection.RIGHT);
    
    this.bonuses.push(bonus)
  }

  private resetGame = () => {
    this.player.reset();
    this.camera.x = 0;
    this.camera.y = 0;
    this.isGameOver = false;
    this.score = 0;
    this.coins = 0;
    this.lives = 3;
    this.remaining = 400;
    this.lastRemainingTime = 0;

    this.generateMap();
    this.generateMergedMap();

    this.fireballs = [];
    this.animateBlocks = [];
    this.generateEnemies();
    this.bonuses = [];
  }
}