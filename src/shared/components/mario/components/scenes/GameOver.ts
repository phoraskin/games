import { IScreen, TSceneName } from '../../index';
import { IControls } from '../Controls';
import Scene from './Scene';

export default class GameOver extends Scene {
  constructor(screen: IScreen, controls: IControls) {
    super(screen, controls);
  }

  render = (time: number): TSceneName | null => {
    if (!this.ctx) return null;

    this.ctx.fillStyle = '#000000';
    this.ctx.fillRect(0, 0, this.canvas.width, this.canvas.height );
    this.ctx.fillStyle = '#ffffff';
    this.ctx.font = '32px Georgia';
    this.ctx.fillText('GAME OVER', this.canvas.width / 2 - 220, this.canvas.height / 2 - 50);
    this.ctx.font = '12px Georgia';
    this.ctx.fillText('press space to start new game', this.canvas.width / 2 - 200, this.canvas.height / 2 - 30);

    if (this.controls.states['fire']) {
      return 'game';
    }

    return 'gameOver';
  }
}