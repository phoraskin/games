import { IScreen, TSceneName } from '../../index';
import { IControls } from '../Controls';
import Scene from './Scene';

export default class GameWin extends Scene {
  private lastTime: number;
  private colors: string[];
  private colorIndex: number;

  constructor(screen: IScreen, controls: IControls) {
    super(screen, controls);

    this.lastTime = 0;
    this.colors = ['#ff0000', '#00ff00', '#0000ff'];
    this.colorIndex = 0;
  }

  render = (time: number): TSceneName | null => {
    if (!this.ctx) return null;

    if (time - this.lastTime > 500) {
      this.lastTime = time;
      if (this.colorIndex === this.colors.length - 1) {
        this.colorIndex = 0;
      } else {
        this.colorIndex++;
      }
    }

    this.ctx.fillStyle = '#ffffff';
    this.ctx.fillRect(0, 0, this.canvas.width, this.canvas.height );
    this.ctx.drawImage(this.imgs['win'], this.canvas.width / 3 - 120, this.canvas.height / 3 - 150, 240, 300);
    this.ctx.fillStyle = this.colors[this.colorIndex];
    this.ctx.font = '32px Georgia';
    this.ctx.fillText('GAME WIN', this.canvas.width / 2 - 220, this.canvas.height / 2 - 50);
    this.ctx.font = '12px Georgia';
    this.ctx.fillText('press space to start new game', this.canvas.width / 2 - 210, this.canvas.height / 2 - 30);

    if (this.controls.states['fire']) {
      return 'game';
    }

    return 'gameWin';
  }
}