import { ISprite } from "./Player";
import { IBlock, isIntersect } from "../utils";
import { IImgs } from "..";

export enum EDirection {
  LEFT, RIGHT
};

export enum EPetState {
  DEFAULT, DEAD
};

export interface IPet {
  render: (ctx: CanvasRenderingContext2D) => void;
  update: (time: number, offsetX: number) => this;
  onKill: () => void;
  getX: () => number;
  getY: () => number;
  setX: (x: number) => void;
  setY: (y: number) => void;
  getWidth: () => number;
  getHeight: () => number;
  setDirection: (direction: EDirection) => void;
  getState: () => EPetState;
  setState: (state: EPetState) => void;
}

export default class Pet {
  protected imgName: string;
  protected speed: number;
  protected sprites: ISprite[];
  protected lastTime: number;
  protected offsetX: number;
  protected x: number;
  protected y: number;
  protected direction: EDirection;
  protected barriers: IBlock[];
  protected imgs: IImgs;
  protected currentAnimationFrame: number;
  protected isFalling: boolean;
  protected state: EPetState;

  constructor(x: number, y: number, barriers: IBlock[], imgs: IImgs) {
    this.imgName = 'characters';
    this.x = x;
    this.y = y;
    this.barriers = barriers;
    this.imgs = imgs;
    this.direction = EDirection.LEFT;
    this.speed = 1;
    this.lastTime = 0;
    this.offsetX = 0;
    this.currentAnimationFrame = 0;
    this.isFalling = false;
    this.state = EPetState.DEFAULT;

    this.sprites = [
      {
        width: 14,
        height: 16,
        directions: [[{sx: 127, sy: 271}], [{sx: 127, sy: 271}],],
      }, {
        width: 14,
        height: 16,
        directions: [[{sx: 127, sy: 271}], [{sx: 127, sy: 271}],],
      },
    ];
  }

  public render = (ctx: CanvasRenderingContext2D) => {
    const sprite = this.getSprite();
    const currentSprite = sprite.directions[this.direction];
    const { width, height } = sprite;
    ctx.drawImage(this.imgs[this.imgName], currentSprite[this.currentAnimationFrame].sx, currentSprite[this.currentAnimationFrame].sy, width, height, this.x - this.offsetX, this.y, width, height);
  }

  public update = (time: number, offsetX: number) => {
    const sprite = this.getSprite();
    const currentSprite = sprite.directions[this.direction];
    const width = sprite.width;
    const height = sprite.height;

    if (time - this.lastTime > 200) {
      this.lastTime = time;
      this.currentAnimationFrame = this.currentAnimationFrame >= currentSprite.length - 1 ? 0 : this.currentAnimationFrame + 1;
    }
    this.offsetX = offsetX;

    const bottomEl = this.barriers.find(el => isIntersect({x: this.x, y: this.y + 5, width, height}, {x: el.x, y: el.y, width: el.width, height: el.height}));
    if (!bottomEl) {
      this.isFalling = true;
      this.y += 5;
    } else {
      if (this.isFalling) {
        this.y = bottomEl.y - height;
      }
      this.isFalling = false;
    }

    const destination = {x: this.x + (this.direction === EDirection.RIGHT ? this.speed : this.speed * -1), y: this.y, width, height};
    const isTouch = this.barriers.some(el => isIntersect(destination, {x: el.x, y: el.y, width: el.width, height: el.height}));
    if (isTouch) {
      this.direction = this.direction === EDirection.RIGHT ? EDirection.LEFT: EDirection.RIGHT;
      destination.x = this.x + (this.direction === EDirection.RIGHT ? this.speed : this.speed * -1);
    }
    this.x = destination.x;
    this.y = destination.y;

    return this;
  }

  public onKill = () => {

  }

  public getX = (): number => {
    return this.x;
  }

  public getY = (): number => {
    return this.y;
  }

  public setX = (x: number) => {
    this.x = x;
  }

  public setY = (y: number) => {
    this.y = y;
  }

  public getWidth = (): number => {
    return this.getSprite().width;
  }
  
  public getHeight = (): number => {
    return this.getSprite().height;
  }

  public setDirection = (direction: EDirection) => {
    this.direction = direction;
  }

  public getState = (): EPetState => {
    return this.state;
  }

  public setState = (state: EPetState): void => {
    this.currentAnimationFrame = 0;
    if (state === EPetState.DEAD) {
      this.speed = 0;
    }
    this.state = state;
  }

  protected getSprite = (): ISprite => {
    return this.sprites[this.state];
  }
}