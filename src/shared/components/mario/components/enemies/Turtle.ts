import Pet, { IPet } from '../Pet';
import { IBlock } from '../../utils';
import { IImgs } from '../..';

export default class Turtle extends Pet implements IPet {
  constructor(x: number, y: number, barriers: IBlock[], imgs: IImgs) {
    super(x, y, barriers, imgs);

    this.sprites = [
      {
        width: 16,
        height: 24,
        directions: [
          [{sx: 201, sy: 206}, {sx: 182, sy: 206}],
          [{sx: 296, sy: 206}, {sx: 315, sy: 206}]
        ],
      }, {
        width: 16,
        height: 14,
        directions: [
          [{sx: 144, sy: 216}],
          [{sx: 144, sy: 216}]
        ],
      },
    ];
  }
  
}