import Pet, { IPet } from '../Pet';
import { IBlock } from '../../utils';
import { IImgs } from '../..';

export default class Mushroom extends Pet implements IPet {
  constructor(x: number, y: number, barriers: IBlock[], imgs: IImgs) {
    super(x, y, barriers, imgs);

    this.sprites = [
      {
        width: 16,
        height: 16,
        directions: [
          [{sx: 296, sy: 187}, {sx: 315, sy: 187}],
          [{sx: 296, sy: 187}, {sx: 315, sy: 187}]
        ],
      }, {
        width: 16,
        height: 8,
        directions: [
          [{sx: 277, sy: 195}],
          [{sx: 277, sy: 195}]
        ],
      }
    ];
  }
  
  onKill = () => {
    
  }
}