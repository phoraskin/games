import Pet, { IPet } from '../Pet';
import { IBlock } from '../../utils';
import { IImgs } from '../..';

export default class Flower extends Pet implements IPet {
  constructor(x: number, y: number, barriers: IBlock[], imgs: IImgs) {
    super(x, y, barriers, imgs);

    this.imgName = 'tiles';
    this.speed = 0;
    this.sprites = [
      {
        width: 16,
        height: 16,
        directions: [
          [{sx: 81, sy: 114},],
          [{sx: 81, sy: 114},]
        ],
      }, {
        width: 16,
        height: 16,
        directions: [
          [{sx: 81, sy: 114},],
          [{sx: 81, sy: 114},]
        ],
      }
    ];
  }
  
}