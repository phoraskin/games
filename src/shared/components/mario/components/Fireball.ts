import { EDirection } from "./Player";
import { isIntersect, IBlock } from '../utils';

export default class Fireball {
  private x: number;
  private y: number;
  private radius: number;
  private speed: number;
  private direction: EDirection;
  private lastTime: number;
  private offsetX: number;
  private canvas: HTMLCanvasElement;
  private selfDestroy: () => void;
  private barriers: IBlock[];

  constructor(x: number, y: number, direction: EDirection) {
    this.x = x;
    this.y = y;
    this.direction = direction;
    this.radius = 5;
    this.speed = 5;
    this.selfDestroy = () => {};
    this.barriers = [];
  }

  public setCanvas = (canvas: HTMLCanvasElement) => {
    this.canvas = canvas;
  }

  public setSelfDestroy = (selfDestroy: () => void) => {
    this.selfDestroy = selfDestroy;
  }

  public setBarriers = (barriers: IBlock[]) => {
    this.barriers = barriers;
  }

  public update = (time: number, offsetX: number) => {
    if (this.x - offsetX > (this.canvas ? (this.canvas.width / 1.5): 4000) || this.x - offsetX < 0) {
      this.selfDestroy();
    }
    if (this.barriers.some(barrier => this.isHit(barrier.x, barrier.y, barrier.width, barrier.height))) {
      this.selfDestroy();
    }
    this.offsetX = offsetX;
    if (time - this.lastTime > 30) {
      this.lastTime = time;
    }
    if (this.direction === EDirection.LEFT) {
      this.x -= this.speed;
    } else {
      this.x += this.speed;
    }
    this.y = this.y;
  }

  public isHit = (dx: number, dy: number, dw: number, dh: number): boolean => {
    const block1 = {x: this.x, y: this.y, width: this.radius * 2, height: this.radius * 2};
    const block2 = {x: dx, y: dy, width: dw, height: dh};

    return isIntersect(block1, block2);
  }

  public render = (ctx: CanvasRenderingContext2D) => {
    ctx.fillStyle = '#ff0000';
    ctx.beginPath();
    ctx.arc(this.x - this.offsetX, this.y, this.radius, 0, Math.PI * 2);
    ctx.closePath();
    ctx.fill();
  }
}