declare const styles: {
  container: string;
  title: string;
  canvas: string;
};

export = styles;