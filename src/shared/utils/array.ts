/**
 * Type guard for filter method. Return all not null, undefined values.
 * @param value input value, which should be filtered
 * @example const array: (string | null)[] = ['foo', 'bar', null, 'zoo', null];
 * const filteredArray: string[] = array.filter(notEmpty);
 */ 
export function notEmpty<T>(value: T | null | undefined): value is T {
  return value !== null && value !== undefined;
}
