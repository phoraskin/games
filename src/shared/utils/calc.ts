export const getBlockWidth = (block: number[][]): number => {
  const xs = block.map(coord => coord[0]);
  const max = Math.max.apply(null, xs);
  const min = Math.min.apply(null, xs);

  return max - min;
}

export const getBlockVisibleWidth = (block: number[][]): number => {
  const xs = block.map(coord => coord[0]);
  const max = Math.max.apply(null, xs);
  const min = Math.min.apply(null, xs);

  if (min < 0) {
    return max > 0 ? max : 0;
  }

  return max - min;
}

export const isIntersectionRects = (main: number[][], ...other: number[][][]): boolean => {
  return other.some(rect => {
    return rect[0][0] >= main[0][0]
      && rect[0][1] >= main[0][1]
      && rect[0][0] <= main[1][0]
      && rect[0][1] <= main[1][1]
      || rect[1][0] >= main[0][0]
      && rect[1][1] >= main[0][1]
      && rect[1][0] <= main[1][0]
      && rect[1][1] <= main[1][1];
  });
}

interface IIntersection {
  id: number;
  rect: number[][] | null;
  type?: 'cactus' | 'pterodactyl';
}
export const getIntersections = (main: number[][], ...other: IIntersection[]): IIntersection[] => {
  return other.filter(intersection => {
    const rect = intersection.rect;
    if (!rect) return false;
    return rect[0][0] >= main[0][0]
      && rect[0][1] >= main[0][1]
      && rect[0][0] <= main[1][0]
      && rect[0][1] <= main[1][1]
      || rect[1][0] >= main[0][0]
      && rect[1][1] >= main[0][1]
      && rect[1][0] <= main[1][0]
      && rect[1][1] <= main[1][1];
  });
}
